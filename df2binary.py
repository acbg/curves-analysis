import pandas as pd
import os
import time

startTime = time.time()

def saveBinaryDF(directory):
    os.chdir(directory)
    for root, dirs, files in os.walk('.'):
        #  print(root, dirs, files)
        for fname in files:
            #  print(time.time()-startTime)
            if fname.endswith('.dat'):
                print(fname)
                df = pd.read_csv(fname, delimiter=',', comment='#')
                #  print(df.columns)

                with open(fname) as f:
                    try:
                        comment = f.readlines()[19]
                        comment = comment.split('# ', 1)[-1]
                        comment = comment.split('\n', 1)[0]
                    finally:
                        f.close()

                df['comments'] = comment
                #  print(df.columns)
                df.to_pickle(fname+'.pkl')
                dg=pd.read_pickle(fname+'.pkl')
                print(dg.comments[0])


saveBinaryDF ('/m/nbe/project/quantum-data/ZIM/notes/iTPX/Samples/G/Aalto3.1/S126/IVdata/')
