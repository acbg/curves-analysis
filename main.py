#/usr/bin/env python3

##################################
# Analysis of experimental curves
##################################

# TODO construct dictionary with plotting variables and their correspondent labels and units:
#         { 'u': ['$U$', 'V'], 'iL': ['$I_L$', 'A'], ... }
# TODO: add physical constants and units capabilities with 'pint'

# Import time module and  start counting time
import time
startTime=time.time()

###############################################################################
# Import modules
###############################################################################
print("*** Elapsed time: {t:.2f} s (Initialize core functions)".format(t=time.time() - startTime))

import pathlib
from pprint import pprint
import numpy as np
np.set_printoptions(threshold=np.inf)
import warnings
warnings.filterwarnings("ignore")
import matplotlib.pyplot as plt



########################
# Physical constants
########################
import sys
# access to all other packages in the upper directory
sys.path.append('../')
from curvesAnalysis import funsAnalysis as f
from physics.constants import ureg, Q_
import physics.constants as pc
import physics.functions as pf


###############
# Import files
###############


# Choose the default root directory:
rootDir = f.getRootDir('/l/Samples/')

# or somewhere in the home directory:
#  homeDir = str(pathlib.Path.home())
#  rootDir = fa.getRootDir(homeDir+'/qdata/ZIM/notes/iTPX/Samples/')

# ... and subdirectories according to the analysis to perform:
case = 'PV' #Analysis of photodetectors
case = 'ContactDistance' # Analysis of the effect of contact distance on DDS
dirList = f.getDirList(case=case)

# Select file extensions to look for:
mask = ['*.dat']

# Or manually choose one directory
#  dirList = f.getDirList(
#          dirArray=['/l/Samples/TPX/TPX0300-TPX0399/TPX0342/S132/IVdata/'])
#  mask = ['1000G1.dat']

#  dirList = f.getDirList(
#          dirArray=['/l/Samples/TPX/TPX0300-TPX0399/TPX0342/S130/IVdata/'])
#  mask = ['500E6.dat']

#  print(dirList)

# And create a file list:
fileList = f.getFiles (rootDir, dirList, mask, showFiles=False)
#  pprint(fileList)


###############################################################################
# Extract data from files
print("*** Elapsed time: {t:.2f} s (Extract data from files)".format(t=time.time() - startTime))
###############################################################################

# Select just one device:
#  device = f.Device(pathlib.Path('/m/nbe/project/quantum-data/ZIM/notes/iTPX/Samples/TPX/TPX0300-TPX0399/TPX0341/S135/IVdata/1000K4.dat'), fileList)

# ... or multiple devices stored in a list:
deviceList = [ f.Device (filename, fileList, printNames=False) for filename in fileList]
#  print(deviceList)

# See times:
'''
deviceList = []
total=0
for ii, filename in enumerate(fileList):
      start = time.time()
      deviceList.append(f.Device(filename, fileList, printNames=False))
      stop = time.time()
      total=total+stop-start
      print(ii, stop-start, total)
#  '''

###############################################################################
# Discard devices
print("*** Elapsed time: {t:.2f} s (Discard devices)".format(t=time.time() - startTime))
###############################################################################
# Manually discarded devices:
f.discardManual(deviceList, case)


###############################################################################
# Create dataframe
print("*** Elapsed time: {t:.2f} s (Create devices DataFrame)".format(t=time.time() - startTime))
###############################################################################
devsDF, devsDFsimple = f.createDF (deviceList, showDF=False)

###############################################################################
# Select best devices
print("*** Elapsed time: {t:.2f} s (Select best devices)".format(t=time.time() - startTime))
###############################################################################

# Drop discarded devices and those with a CQE higher or equal to 1:
devsDFfilt = devsDF[(devsDF['cqeM'] < 1) & (devsDF['discard'] == False)]

'''
print(
        devsDFfilt[[
            'sampleName', 'samplePID', 'name',
            'discard', 'has4pp',
            'cqem', 'Rs',
            #  'markerSty',
            #  'lineSty',
            ]]
    )
#  '''

'''
print(
        devsDFfilt[[
            'sampleName', 'samplePID', 'name',
            'discard',
            'cqem', 'Rs',
            ]][
                devsDFfilt['name']=='250E0'
            ]
    )
#  '''

# Select devices according to two different variables (this can be done independently in each analysis):
'''
bestDevsDF = f.selDevs2vars (
        devsDFfilt,
        ['name', 'samplePID'],
        #  'cqem4pp', 'cqem4pp',
        'cqem', 'cqem',
        'sampleName', 'diameter', 'dT', 'dM',
        #  'sampleName', 'diameter', 'dT', 'dM'
        showDF=False,
        )
#  '''
# And create a dictionary with the selection:
'''
bestDevs = f.df2dict ( df = bestDevsDF,
        maingroup='sampleName', subgroup='samplePID', selectedCol='name')
#  '''

##############################################################################
print("*** Elapsed time: {t:.2f} s (End of core functions)".format(t=time.time() - startTime))
##############################################################################
