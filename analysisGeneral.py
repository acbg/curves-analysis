# ########################
# Analysis: General plots
# ########################
import time as tm
st = tm.time () # Start counting time

#---------------------------------------------------------------------------------------
print(f'*** Elapsed time: {tm.time() - st:.2f} s (Analysis: General)')
#---------------------------------------------------------------------------------------

import re
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns

import funsAnalysis as f

########################
# Physical constants
########################
import sys
# access to all other packages in the upper directory
sys.path.append('../')
from physics.constants import ureg, Q_
import physics.constants as pc
import physics.functions as pf

#  Matplotlib settings
#  --------------------
#  print(plt.style.available)

plt.rcParams.update(plt.rcParamsDefault)
#  plt.style.use('dark_background')
#  plt.style.use('seaborn-dark')
plt.rcParams.update({
    'font.size': 7,
    'font.family': 'serif',
    #  'xtick.labelsize' : 17,
    #  'ytick.labelsize' : 17,
    })


# Create a device list and a dataframe
# ------------------------------------
dirList=['/l/Samples/TPX/TPX0300-TPX0399/TPX0342/S130/IVdata/']

devList, devsDF = \
        f.createDevList(
            st,
            rootDir='/l/Samples/',
            #  case='ContactDistance',
            #  dirList=[],
            dirList=dirList,
            #  mask = ['500D1.dat','500E6.dat'],
            #  mask = ['500E6.dat'],
            printFileList=0
        )


########
# Plots
########
print(f"*** Elapsed time: {tm.time() - st:.2f} s (Plot figures)")

# Plot each device's IV and CQE plots
# -----------------------------------

#  '''
for dev in devList:
    print(dev.filename)
    fig=dev.plotIVandCQE(showPlot=True)
    plt.show()
#  '''


# Plot all devices from one sample with certain properties
# --------------------------------------------------------
#  '''
selSample = 'Aalto3.1'
selSample = 'TPX0341'
selSample = 'TPX0342'
selDia = 500 * ureg.micron
seldT = 50 * ureg.micron

fig, ax = plt.subplots(2,2)

for dev in devList:

    if dev.sampleName == selSample \
        and dev.diameter == selDia \
        and dev.dT == seldT:

        ax[0,0].semilogy(
                dev.u, dev.iL, '-',
                marker = dev.markerSty,
                #  ls = dev.lineSty,
                color = dev.colorSty,
                label = f'{dev.sampleName}|{dev.samplePID}|{dev.name}-LED: $d_T={dev.dT:.0f~L}, d_M={dev.dM:.0f~L}$'
            )

        ax[0,0].semilogy(
                dev.u, dev.iD, ':',
                #  marker = dev.markerSty,
                #  ls = dev.lineSty,
                color = dev.colorSty,
                label = f'{dev.sampleName}|{dev.samplePID}|{dev.name}-PD: $d_T={dev.dT:.0f~L}, d_M={dev.dM:.0f~L}$'
            )

        ax[0,1].scatter(
                dev.u, dev.cqe,
                marker = dev.markerSty,
                #  ls = dev.lineSty,
                color = dev.colorSty,
                label = f'{dev.sampleName}|{dev.samplePID}|{dev.name}: $d_T={dev.dT:.0f~L}, d_M={dev.dM:.0f~L}$'
            )

        ax[0,1].plot(
                dev.u, dev.cqeOrig, ':',
                #  marker = dev.markerSty,
                #  ls = dev.lineSty,
                color = dev.colorSty,
                label = f'{dev.sampleName}|{dev.samplePID}|{dev.name}: $d_T={dev.dT:.0f~L}, d_M={dev.dM:.0f~L}$ Sm'
            )

        ax[1,0].scatter(
                dev.iL, dev.cqe,
                marker = dev.markerSty,
                #  ls = dev.lineSty,
                color = dev.colorSty,
                label = f'{dev.sampleName}|{dev.samplePID}|{dev.name}: $d_T={dev.dT:.0f~L}, d_M={dev.dM:.0f~L}$'
            )

        ax[1,0].plot(
                dev.iL, dev.cqeOrig, ':',
                #  marker = dev.markerSty,
                #  ls = dev.lineSty,
                color = dev.colorSty,
                label = f'{dev.sampleName}|{dev.samplePID}|{dev.name}: $d_T={dev.dT:.0f~L}, d_M={dev.dM:.0f~L}$ Sm'
            )
ax[0,0].legend(ncol=2, loc='best')
#  ax[0].axis([-1, 5, 0, 1])
ax[0,1].legend(ncol=2, loc='best')
ax[0,1].axis([1, 4, 0, 1])
ax[1,0].legend(ncol=2, loc='best')
ax[1,0].axis([0, .3, 0, 1])
plt.savefig('output/exp-'+selSample+'-'+str(selDia.magnitude)+'-stats.jpg')

plt.show()
#  '''


# Plot two variables for all devices with a different color for one variable
# --------------------------------------------------------------------------
#  '''
bestDevsDF = f.selDevs2vars (
        df=devsDF, outVar=['name', 'samplePID'],
        maxVar='cqeM', minVar='Rs',
        #  maxVar='cqem4pp', minVar='Rs4pp',
        groupVars=['sampleName', 'diameter', 'dT', 'dM'],
        showDF=False,
        )

f.plot2vars (devList, bestDevsDF,
        xVars=['u', 'u'],
        yVars=['cqe', 'iL'],
        colorVar='dT',
        xLims=[[.8, 3], [0.8, 3.0]],
        yLims=[[0, 0.8], [0.0, 0.8]],
        #  **xLims=[{}, {'left':0.8}],
        #  **yLims=[{}, {'bottom':0, 'top':0.8}],
        )
plt.show()
#  '''

'''
# Plot Statistics on certain value
# --------------------------------
#  print(devsDFfilt.columns)
#  g = sns.lmplot ('diameter', 'cqeM', devsDFfilt)
#  plt.show()
f.plotStatistics(dataFrame=devsDF, x='dM', y='cqeM', hue='dT', columnsData='diameter')
'''

#---------------------------------------------------------------------------------------
print(f"*** Elapsed time: {tm.time() - st:.2f} s (Plotted all figures)")
#---------------------------------------------------------------------------------------

plt.show()

#---------------------------------------------------------------------------------------
print(f"*** Elapsed time: {tm.time() - st:.2f} s (End of analysis)")
#---------------------------------------------------------------------------------------
