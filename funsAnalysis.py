import time as tm
import os
import pathlib
import copy
from tabulate import tabulate # print nice tables
import re # regular expressions for strings
import numpy as np
np.set_printoptions(threshold=np.inf)
import warnings
warnings.filterwarnings("ignore")
import scipy
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
#  from mpl_toolkits.mplot3d import Axes3D
from pprint import pprint

########################
# Physical constants
########################
import sys
# access to all other packages in the upper directory
sys.path.append('./')
sys.path.append('../')
import pint
from physics.constants import ureg, Q_

import physics.constants as pc
import physics.functions as pf

#####################
# Files manipulation
#####################

def createDevList(startTime, rootDir=None, case=None, dirList = [], mask=['*.dat'],
        printDirLIst=False, printFileList=False, printDF = False,
        ):
    '''
    '''

    st = startTime
    print(f"*** Elapsed time: {tm.time() - st:.2f} s (Init device list creation)")
    # Import files
    ###############
    # Choose the default root directory:
    if not rootDir:
        rootDir = getRootDir()

    # ... and subdirectories according to the analysis to perform:
    #  if case:
        #  dirList = getDirList(case=case)

    if printDirLIst:
        pprint(dirList)

    # And create a file list:
    fileList = getFiles (rootDir, dirList, mask, showFiles=False)
    if printFileList:
        pprint(fileList)

    # Extract data from files
    ##########################
    print(f"*** Elapsed time: {tm.time() - st:.2f} s (Extract data from files)")

    # Select multiple devices and store them in a list:
    devList = [ Device (filename, fileList, printNames=False) for filename in fileList]

    # TODO: a function to see the time spent for each device creation

    # Discard devices
    ##################
    print(f"*** Elapsed time: {tm.time() - st:.2f} s (Discard devices)")
    # Manually discarded devices:
    if case:
        discardManual(devList, case)

    # Create dataframe
    ###################
    print(f"*** Elapsed time: {tm.time() - st:.2f} s (Create devices DataFrame)")
    devsDF = createDF (devList, showDF=printDF)
    #  devsDF = devsDF[(devsDF['cqeM'] < 1) & (devsDF['discard'] == False)]

    # Select best devices
    ######################
    print(f"*** Elapsed time: {tm.time() - st:.2f} s (Select best devices)")

    return devList, devsDF

def getRootDir(directory=None):
    '''
    Set the root directory.
    If no argument is given, it gives the default /m/nbe/..., and if that doesn't exist,
    it gives the same route mounted in the home directory.
    '''
    if directory:
        return directory
    else:
        defaultDir = '/m/nbe/project/quantum-data/ZIM/notes/iTPX/Samples/'
        homeDefaultDir = str(pathlib.Path.home()) + '/qdata/ZIM/notes/iTPX/Samples/'
        if pathlib.Path(defaultDir).exists():
            return defaultDir
        elif pathlib.Path(homeDefaultDir).exists():
            return homeDefaultDir
        else:
            print('### The root directory does not exist!')
            return

def getDirList(dirArray = None, case=None):
    '''
    Gives a directory list corresponding to the case being performed
    '''

    if case == None:

        dirList = dirArray

    if case == 'ContactDistance':

        dirList = [
                'G/Aalto3.1/S125/IVdata/',
                'G/Aalto3.1/S126/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0341/S129/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0341/S134/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0341/S135/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0342/S130/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0342/S132/IVdata/',
            ]

    if case == 'Passivation':

        dirList = [
                'TPX/TPX0300-TPX0399/TPX0341/S134/passivation/',
                'TPX/TPX0300-TPX0399/TPX0341/S135/passivation/',
            ]

    if case == 'Tdep':

        dirList = [
                'TPX/TPX0300-TPX0399/TPX0341/S134/tdep/',
            ]


    if case == 'PV':

        dirList = [
                'K/K4733/K4733-S83/data/',
                'TPX/TPX0300-TPX0399/TPX0341/S129/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0342/S132/IVdata/',
            ]

    return dirList

def getFiles(rootDir, dirs, extensions, showFiles=False):
    '''
    Get file names with different extensions from a set of directories contained in a root directory
    '''
    all_files = []

    #  dires = []
    #  for directory in dirs:
    #      print(directory)
    #      dires.extend (pathlib.Path(rootDir+directory))
    #      print(dires)

    origDir = os.getcwd()
    os.chdir(rootDir)
    for d in dirs:
        for ext in extensions:
            all_files.extend(pathlib.Path(rootDir,d).glob(ext))
    os.chdir(origDir)

    #  for dirName, subdirList, fileList in os.walk(rootDir):
    #      print('Found directory: %s' % dirName)
    #      for fname in fileList:
    #          print('\t%s' % fname)
    #      for subdir,subsubdirList, subfileList in os.walk(subdirList):
    #          for fname in fileList:
    #              print('\t%s' % fname)

    if showFiles:
        for item in all_files: print(item)

    return all_files

#######################################
# Device class and associated functions
#######################################
class Device (object):

    discardManual = False
    counter = 0
    lines = iter (1000 * ['-','--','-.',':'])
    #  markers = iter (1000 * list(mpl.lines.lineMarkers.keys()))
    markers = iter (1000 * ['o','*','^','v','<','x','>','p','s','h','H','D','d'])
    #  colormap = plt.cm.Spectral
    #  colors = iter (1000 * [colormap(x) for x in range(colormap.N)])
    colors = iter (1000 * [plt.cm.Spectral(x) for x in range(plt.cm.Spectral.N)])

    def __init__ (
            self, filename, filelist,
            material = 'GaAs',
            ARthickness = Q_(300, 'nm'),
            T = Q_(300, 'K'),
            printNames = False
        ):

        Device.counter += 1

        self.filename = filename
        if printNames:
            print(self.filename)
        self.filelist = filelist
        self.material = material
        self.ARthickness = ARthickness
        self.T = T

        self.lineSty = next (self.lines)
        self.markerSty = next (self.markers)
        self.colorSty = next (self.colors)

        self.sampID = self.filename.parent.as_posix().split('IVdata')[0]
        self.samplePID = self.sampID.split('/')[-2]
        self.sampleName = self.sampID.split('/')[-3]

        self.devID = self.filename.stem # Device complete name

        self.name = re.findall('[a-zA-Z0-9]+[A-Z0-9]', self.devID)[0]

        #  self.isDDS = False if np.mean(self.iD)>1e3 else True
        self.isDDS = False if self.name.endswith('P') else True #if it's a solar cell
        self.isPD  = True if self.name.endswith('P') else False #device is a photodetector

        diam = re.split('[A-Z]+', self.devID)[0]
        self.diameter = Q_ (float(diam), 'microns') # in meters
        self.radius = self.diameter / 2
        self.area = np.pi * (self.radius)**2

        self.meas = re.findall('[a-zA-Z]', self.devID)[-1]

        if 'T' in self.devID:
            temperature = devID.split('T')[1]
            self.T = Q_ (float (re.sub ('\D', '', temperature)), 'K')

        self.ni = pf.ni(self.material, self.T)

        #  Read csv file and add the IV data to a dataframe
        if filename.suffix == '.dat':
            self.df = pd.read_csv(filename, delimiter=',', comment='#', header=[0])
            #  Get parameters from filename and store them in ndarray
            with self.filename.open() as f:
                try:
                    comment = f.readlines()[19]
                    comment = comment.split('# ', 1)[-1]
                    comment = comment.split('\n', 1)[0]
                finally:
                    f.close()

        elif filename.suffix == '.feather':
            self.df = pd.read_feather(filename)
        elif filename.suffix == '.h5':
            self.df = pd.read_hdf(filename)
        elif filename.suffix == '.pkl':
            filename = str(filename)
            self.df = pd.read_pickle(filename)

        self.comments = comment

        #  print(self.comments)

        # Does the file contain 4pp measurements?
        self.has4pp = True \
                if self.comments == '#Both 2 and 4 wire measurements' \
                or self.comments == '#4 wire measurements'\
                else False

        self.u       =  np.zeros(len(filelist))
        self.uD      =  np.zeros(len(filelist))
        self.iL      =  np.zeros(len(filelist))
        self.iD      =  np.zeros(len(filelist))
        # -----------------------------
        self.u4pp    =  np.zeros(len(filelist))
        self.uD4pp   =  np.zeros(len(filelist))
        self.iL4pp   =  np.zeros(len(filelist))
        self.iD4pp   =  np.zeros(len(filelist))
        # -----------------------------
        self.uc      =  np.zeros(len(filelist))
        self.uDc     =  np.zeros(len(filelist))
        self.iLc     =  np.zeros(len(filelist))
        self.iDc     =  np.zeros(len(filelist))
        # -----------------------------
        self.u4ppc   =  np.zeros(len(filelist))
        self.uD4ppc  =  np.zeros(len(filelist))
        self.iL4ppc  =  np.zeros(len(filelist))
        self.iD4ppc  =  np.zeros(len(filelist))

        # Store data in separated numpy ndarrays
        if ' VS' in self.df:
            self.u  =  self.df[' VS'].values
            self.uD =  self.df[' VD'].values
            self.iL =  self.df['IS'].values if self.df['IS'].values[-1] > 0 else -self.df['IS'].values
            self.iD =  self.df[' ID'].values if self.df[' ID'].values[-1] > 0 else -self.df[' ID'].values
            # -----------------------------
            self.u4pp  =  self.df[' VS'].values
            self.uD4pp =  self.df[' VD'].values
            self.iL4pp =  self.df['IS'].values if self.df['IS'].values[-1] > 0 else -self.df['IS'].values
            self.iD4pp =  self.df[' ID'].values if self.df[' ID'].values[-1] > 0 else -self.df[' ID'].values

        elif ' VS_pulse' in self.df:
            self.u  =  self.df[' VS_pulse'].values
            self.uD =  self.df[' VD_pulse'].values
            self.iL =  self.df['IS_pulse'].values if self.df['IS_pulse'].values[-1] > 0 else -self.df['IS_pulse'].values
            self.iD =  self.df[' ID_pulse'].values if self.df[' ID_pulse'].values[-1] > 0 else -self.df[' ID_pulse'].values
            # -----------------------------
            self.uc  =  self.df[' VS_cont'].values
            self.uDc =  self.df[' VD_cont'].values
            self.iLc =  self.df[' IS_cont'].values if self.df[' IS_cont'].values[-1] > 0 else -self.df[' IS_cont'].values
            self.iDc =  self.df[' ID_cont'].values if self.df[' ID_cont'].values[-1] > 0 else -self.df[' ID_cont'].values

        elif ' VS_pulse_4pr' in self.df:
            self.u  =  self.df[' VS_pulse_2pr'].values
            self.uD =  self.df[' VD_pulse_2pr'].values
            self.iL =  self.df[' IS_pulse_2pr'].values if self.df[' IS_pulse_2pr'].values[-1] > 0 else -self.df[' IS_pulse_2pr'].values
            self.iD =  self.df[' ID_pulse_2pr'].values if self.df[' ID_pulse_2pr'].values[-1] > 0 else -self.df[' ID_pulse_2pr'].values
            # -----------------------------
            self.uc  =  self.df[' VS_cont_2pr'].values
            self.uDc =  self.df[' VD_cont_2pr'].values
            self.iLc =  self.df[' IS_cont_2pr'].values if self.df[' IS_cont_2pr'].values[-1] > 0 else -self.df[' IS_cont_2pr'].values
            self.iDc =  self.df[' ID_cont_2pr'].values if self.df[' ID_cont_2pr'].values[-1] > 0 else -self.df[' ID_cont_2pr'].values
            # -----------------------------
            self.u4pp  =  self.df[' VS_pulse_4pr'].values
            self.uD4pp =  self.df[' VD_pulse_4pr'].values
            self.iL4pp =  self.df['IS_pulse_4pr'].values if self.df['IS_pulse_4pr'].values[-1] > 0 else -self.df['IS_pulse_4pr'].values
            self.iD4pp =  self.df[' ID_pulse_4pr'].values if self.df[' ID_pulse_4pr'].values[-1] > 0 else -self.df[' ID_pulse_4pr'].values
            # -----------------------------
            self.u4ppc  =  self.df[' VS_cont_4pr'].values
            self.uD4ppc =  self.df[' VD_cont_4pr'].values
            self.iL4ppc =  self.df[' IS_cont_4pr'].values if  self.df[' IS_cont_4pr'].values[-1] > 0 else  -self.df[' IS_cont_4pr'].values
            self.iD4ppc =  self.df[' ID_cont_4pr'].values if self.df[' ID_cont_4pr'].values[-1] > 0 else -self.df[' ID_cont_4pr'].values

        ############################################################################

        # Remove jumps
        self.iLorig = copy.deepcopy (self.iL)
        self.iDorig = copy.deepcopy (self.iD)
        self.iL = jumpRemove(self.iL, 1e-1)
        self.iD = jumpRemove(self.iD, 1e-1)
        #  self.iL = smooth (self.iL, 4)
        #  self.iD = smooth (self.iD, 4)
        self.iD4pp = jumpRemove(self.iD4pp, 1e-1)
        self.iL4pp = jumpRemove(self.iL4pp, 1e-1)
        self.iDc = jumpRemove(self.iDc, 1e-1)
        self.iLc = jumpRemove(self.iLc, 1e-1)
        self.iD4ppc = jumpRemove(self.iD4ppc, 1e-1)
        self.iL4ppc = jumpRemove(self.iL4ppc, 1e-1)

        # Set units
        self.iLorig = self.iLorig * ureg.ampere
        self.iDorig = self.iDorig * ureg.ampere
        self.u       =  self.u       * ureg.volt
        self.uD      =  self.uD      * ureg.volt
        self.iL      =  self.iL      * ureg.ampere
        self.iD      =  self.iD      * ureg.ampere
        # --------------# ----------
        self.u4pp    =  self.u4pp    * ureg.volt
        self.uD4pp   =  self.uD4pp   * ureg.volt
        self.iL4pp   =  self.iL4pp   * ureg.ampere
        self.iD4pp   =  self.iD4pp   * ureg.ampere
        # --------------# ----------
        self.uc      =  self.uc      * ureg.volt
        self.uDc     =  self.uDc     * ureg.volt
        self.iLc     =  self.iLc     * ureg.ampere
        self.iDc     =  self.iDc     * ureg.ampere
        # --------------# ----------
        self.u4ppc   =  self.u4ppc   * ureg.volt
        self.uD4ppc  =  self.uD4ppc  * ureg.volt
        self.iL4ppc  =  self.iL4ppc  * ureg.ampere
        self.iD4ppc  =  self.iD4ppc  * ureg.ampere

        # Conductance
        # -----------
        diL = np.diff(self.iL.to_base_units())
        du = np.diff(self.u.to_base_units())
        self.conductance = diL / du if len(self.iL) > 2 else \
                np.zeros (len (self.iL) + 1)
        #  self.conductance = np.gradient(self.iL,self.u)
        self.conductance = self.conductance #* ureg.ampere / ureg.volt
        self.conductanceMax = np.nanmax(self.conductance) #* ureg.ampere / ureg.volt

        diL4pp = np.diff (self.iL4pp.to_base_units())
        du4pp = np.diff (self.u4pp.to_base_units())
        self.conductance4pp = diL4pp / du4pp if len(self.iL4pp) > 2 else \
                np.zeros(len(self.iL4pp)+1)
        self.conductance4pp = self.conductance4pp #* ureg.ampere / ureg.V
        self.conductanceMax4pp = np.nanmax(self.conductance4pp) #* ureg.ampere / ureg.volt

        # Resistance
        # ----------
        self.resistance = 1 / self.conductance
        self.Rs = 1 / self.conductanceMax if not self.conductanceMax == 0\
                else np.nan * ureg.ohm

        self.resistance4pp = 1 / self.conductance4pp
        self.Rs4pp = 1 / self.conductanceMax4pp if not self.conductanceMax4pp == 0 \
                else np.nan * ureg.ohm

        # Ideality factor
        # ---------------
        dx = np.diff (np.log (abs (self.iL.to_base_units().magnitude))) / du
        self.Vt = pf.Vt (self.T).to_base_units()
        self.idealityF = 1 / dx / self.Vt if len (self.iL) > 2 \
                else np.zeros (len (self.iL) - 1)
        self.ideality = np.nanmax (
                        (self.u[:-1] > Q_(1.2, 'V')) *\
                        (self.u[:-1] < Q_(1.6, 'V')) *\
                        (self.idealityF < 3.0 * self.idealityF.units) *\
                        self.idealityF)  if len (self.iL) > 2\
                        else np.NaN

        dx4pp = np.diff (np.log (abs (self.iL4pp.to_base_units().magnitude))) / du4pp
        self.idealityF4pp = 1 / dx4pp / self.Vt if len(self.iL4pp) > 2 \
                else np.zeros (len (self.iL4pp) - 1)
        self.ideality4pp = np.nanmax (
                (self.u4pp[:-1] > Q_(0.9, 'V')) *\
                (self.u4pp[:-1] < Q_(1.1, 'V')) *\
                (self.idealityF4pp < 3.0 * self.idealityF4pp.units) *\
                self.idealityF4pp) if len (self.iL4pp) > 2\
                else np.NaN

        # Internal bias
        # -------------
        self.uL = self.u - self.iL * self.Rs

        self.uL4pp = self.u4pp - self.iL4pp * self.Rs4pp

        # From the PD
        self.uLPD = 1.20*ureg.volt + pc.k * self.T * np.log(self.iD.magnitude) / pc.q

        # ABC parameters
        # --------------
        kkk = pc.q * self.ni * self.ARthickness
        self.iLmin = np.nanmin(np.abs(self.iL))
        srh = (self.u > Q_(0.5, 'V')) * \
            (self.iL > 1.5 * self.iLmin) *\
            self.iL / np.exp (np.abs (self.u.magnitude)) / 2 / self.Vt / kkk
        self.A = np.nanmax (srh)

        self.iL4ppmin = np.nanmin(np.abs(self.iL4pp))
        srh4pp = (self.u4pp > Q_ (0.5, 'V')) *\
            (self.iL4pp > 1.5 * self.iL4ppmin) *\
            self.iL4pp / np.exp (np.abs (self.u4pp.magnitude)) / 2 / self.Vt / kkk
        self.A4pp = np.nanmax (srh4pp)

        # Chi
        x = np.sqrt (abs(self.iD.magnitude))
        y = abs(self.iL.magnitude) / x
        if not np.isnan(y[0]):
            chipfit = np.polyfit (x, y, 1)[0]
            self.chi = 1/chipfit
        else:
            self.chi = np.nan
        self.chi = self.chi * ureg.dimensionless

        # CQE
        self.cqe = self.iD / self.iL if self.isDDS else np.nan
        #  self.cqe = smooth(self.cqe, 4)
        self.cqeOrig = self.iDorig / self.iLorig
        #  self.cqe = smooth(self.cqe.magnitude,4) * ureg.dimensionless if self.isDDS else np.nan
        self.cqec = self.iDc / self.iLc if self.isDDS else np.nan
        self.cqe4pp = self.iD4pp / self.iL4pp if self.isDDS else np.nan

        # CQE filtered (used for finding the peak CQE)
        umin = Q_(1.0, 'V')
        urange = self.u > umin
        self.cqeFilt = urange * self.cqe
        self.uFilt = urange * self.u
        self.iLFilt = urange * self.iL
        uLrange = self.u > umin
        self.uLFilt = uLrange * self.uL

        umin4pp = Q_(1.0, 'V')
        urange4pp = self.u4pp > umin4pp
        self.cqeFilt4pp =  urange4pp * self.cqe4pp

        # CQE maximum (peak)
        self.cqeM = np.nanmax (self.cqeFilt)
        self.cqeMidx = np.argmax (self.cqeFilt)
        self.cqeMi = self.iLFilt [self.cqeMidx]
        self.cqeMu = self.uFilt [self.cqeMidx]
        self.cqeMuL = self.uLFilt [self.cqeMidx]

        self.cqeM4pp = np.nanmax (self.cqeFilt4pp)
        self.cqeMidx4pp = np.argmax (self.cqeFilt4pp)
        self.cqeMi4pp = self.iL4pp [self.cqeMidx4pp - 1]
        self.cqeMu4pp = self.u4pp [self.cqeMidx4pp - 1]

        # CQE at Eg
        uminG, umaxG = Q_(1.0, 'V'), Q_(1.42, 'V')
        urangeG = (self.u > uminG) * (self.u < umaxG)
        self.cqeGfilt = self.cqe * urangeG
        self.cqeG = np.nanmax (self.cqeGfilt)
        self.cqeGidx = np.argmax (self.cqeGfilt)
        self.cqeGi = self.iL [self.cqeGidx]
        self.cqeGuL = self.uL [self.cqeGidx]

        uminG4pp, umaxG4pp = Q_(1.0, 'V'), Q_(1.42, 'V')
        urangeG4pp = (self.u4pp > uminG4pp) * (self.u4pp < umaxG4pp)
        self.cqeGfilt4pp = self.cqe4pp * urangeG4pp
        self.cqeG4pp = np.nanmax (self.cqeGfilt4pp)
        self.cqeG4ppidx = np.argmax (self.cqeGfilt4pp)
        self.cqeGi4pp = self.iL4pp [self.cqeG4ppidx]
        self.cqeGu4pp = self.u4pp [self.cqeG4ppidx]

        #TODO: find the cqeG from an interpolated cqe curve, so that it can be found at exactly 1.42 V

        # PCE
        # ---
        self.pce = self.cqe * pf.Eg(self.material, self.T) / self.u if self.isDDS else np.nan
        self.pce4pp = self.cqe4pp * pf.Eg(self.material, self.T) / self.u4pp if self.isDDS else np.nan

        self.pceFilt = urange * self.pce
        self.pceM = np.nanmax (self.pceFilt)

        self.pceFilt4pp = urange4pp * self.pce4pp
        self.pceM4pp = np.nanmax (self.pceFilt4pp)

        # I_reverse and I_forward
        # -----------------------
        self.iRev = self.iL[0]
        self.iFor = self.iL[-1]

        # A 'discard' array, based on:
        # -----------------------------------
        # TODO: if isDDS, else no discard for non-DDS devices
        # very high reverse (leakage) current,
        self.discard = False
        self.discard = True if abs(self.iRev) > Q_(1e-7, 'A') else self.discard
        # very low forward current,
        self.discard = True if abs(self.iFor) < Q_(1e-2, 'A') else self.discard
        #  # extremely low cqem
        self.discard = True if abs(self.cqeM) < 0.40 else self.discard
        self.discard = True if abs(self.cqeM4pp) < 0.35 else self.discard
        # extremely high cqem
        self.discard = True if abs(self.cqeM) > 0.90 else self.discard
        self.discard = True if abs(self.cqeM4pp) > 0.90 else self.discard
        # very high Rs
        self.discard = True if abs(self.Rs) > Q_(50, 'ohm') else self.discard

        # An attribute to determine if the device is discarded automatically:
        self.discardAuto = self.discard


        # Device mask generation
        # ----------------------
        self.MaskGen = 3 if \
                self.samplePID == 'S126' or\
                self.samplePID == 'S129' or\
                self.samplePID == 'S130' or\
                self.samplePID == 'S134'\
            else 2

        # Device contact distances
        # ------------------------
        self.dT = Q_(5, 'micron') if self.MaskGen == 2 and re.search('[a-hA-H][0-4]', self.name) \
            else Q_(10, 'micron') if self.MaskGen == 2 and re.search('[a-hA-H][5-9]', self.name) \
            else Q_(20, 'micron') if self.MaskGen == 2 and re.search('[i-lI-L][0-4]', self.name)\
            else Q_(40, 'micron') if self.MaskGen == 2 and re.search('[i-lI-L][5-9]', self.name)\
            else Q_(50, 'micron') if self.MaskGen == 3 and re.search('[a-bA-B][0-9]', self.name)\
            else Q_(75, 'micron') if self.MaskGen == 3 and re.search('[c-dC-D][0-9]', self.name)\
            else Q_(95, 'micron') if self.MaskGen == 3 and re.search('[e-fE-F][0-9]', self.name)\
            else Q_( 0, 'micron')

        self.dM = Q_(5, 'micron') if self.MaskGen == 2 and re.search ('[a-hA-H][0-4]', self.name)\
            else Q_(10, 'micron') if self.MaskGen == 2 and re.search ('[a-hA-H][5-9]', self.name)\
            else Q_(20, 'micron') if self.MaskGen == 2 and re.search ('[i-lI-L][0-4]', self.name)\
            else Q_(40, 'micron') if self.MaskGen == 2 and re.search ('[i-lI-L][5-9]', self.name)\
            else Q_(25, 'micron') if self.MaskGen == 3 and re.search ('[a-bA-B][0-4]', self.name)\
            else Q_(50, 'micron') if self.MaskGen == 3 and re.search ('[a-bA-B][5-9]', self.name)\
            else Q_(25, 'micron') if self.MaskGen == 3 and re.search ('[c-dC-D][0-4]', self.name)\
            else Q_(75, 'micron') if self.MaskGen == 3 and re.search ('[c-dC-D][5-9]', self.name)\
            else Q_( 5, 'micron') if self.MaskGen == 3 and re.search ('[e-fE-F][0-4]', self.name)\
            else Q_(95, 'micron') if self.MaskGen == 3 and re.search ('[e-fE-F][5-9]', self.name)\
            else Q_( 0, 'micron')

        self.dC = self.dT + self.dM # total distance between contacts
        self.dT2rad = self.dT / self.radius

        # Current density
        # ---------------
        self.areaLED = (np.pi * (self.radius - self.dT)**2).to_base_units() # LED area
        self.areaPD = (np.pi * (self.radius)**2).to_base_units() # PD area
        self.areaPD = (np.pi * (self.radius - self.dT)**2).to_base_units() # PD area

        self.jL = self.iL / self.areaLED
        self.jD = self.iD / self.areaPD

        # 4pp
        self.jL4pp = self.iL4pp / self.areaLED
        self.jD4pp = self.iD4pp / self.areaPD


    def plotIV (self, plot4pp=False, showEach=False):
        if plot4pp and self.has4pp:
            x  = self.u4pp
            y1 = self.iL4pp
            y2 = self.iD4pp
        else:
            x  = self.u
            y1 = self.iL
            y2 = self.iD
            y3 = self.iLorig
            y4 = self.iDorig

        fig = plt.figure()
        fig.suptitle ('Sample:{self.sampleName}, PID:{self.samplePID}, Device:{self.name}')
        ax = fig.add_subplot(111)
        ax.semilogy (x, y1, 'o', label='LED')
        ax.semilogy (x, y2, 's', label='PD')
        ax.semilogy (x, y3, 'x', label='LED orig')
        ax.semilogy (x, y4, '+', label='PD orig')

        ax.legend(loc='best')

        if showEach:
            plt.show()

        return fig, ax

    def plotCQE (self, plot4pp=False, showEach=False):

        if self.isDDS:

            if plot4pp and self.has4pp:

                title = 'Sample:{sample}, PID:{pid}, Device:{device} \n (4pp)'.format(sample=self.sampleName, pid=self.samplePID, device=self.name)

                x  = self.u4pp
                y1 = self.cqe4pp
                #  y2 = self.cqeFilt4pp
                #  y3 = self.cqexfilt4pp

                lbl1 = f'$CQE$'
                #  lbl2 = f'$CQE_{{filt}}: CQE_M = {self.cqeM4pp: .2f} \; @ V = {self.cqeMu4pp: .2f~L}, (I = {self.cqeMi4pp.to(ureg.mA): .0f~L})$'
                #  lbl3 = f'$CQE_{{G, filt}}: CQE_G = {self.cqeG4pp: .2f} \;  @ V = {self.cqeGu4pp: .2f~L}, (I = {self.cqeGi4pp.to(ureg.mA): .0f~L})$'

                xlims = [0.5, 2]
                ylims = [0.0, 1]

            else:

                title = 'Sample:{sample}, PID:{pid}, Device:{device}'.format(sample=self.sampleName, pid=self.samplePID, device=self.name)

                x  = self.u
                y1 = self.cqe
                #  y2 = self.cqeFilt
                #  y3 = self.cqeGfilt
                y4 = self.cqeOrig

                lbl1 = f'$CQE$'
                #  lbl2 = f'$CQE_{{filt}}: CQE_M = {self.cqeM: .2f} @ {self.cqeMu: .2f~L}, {self.cqeMi.to(ureg.mA): .0f~L}$'
                #  lbl3 = f'$CQE_{{G}}: CQE_G = {self.cqeG: .2f} @ {self.cqeGu: .2f~L}, {self.cqeGi.to(ureg.mA): .0f~L}$'
                lbl4 = f'$CQE orig$'

                xlims = [0.5, 4]
                ylims = [0.0, 1]

            fig = plt.figure()
            fig.suptitle (title)
            ax = fig.add_subplot(111)
            line1, = ax.plot (x, y1, 'o', label=lbl1)
            #  line2, = ax.plot (x, y2, '--', label=lbl2)
            #  line3, = ax.plot (x, y3, ':', label=lbl3)
            line3, = ax.plot (x, y4, 'x', label=lbl4)
            ax.set_xlim (xlims)
            ax.set_ylim (ylims)
            ax.legend(loc='best')

            if showEach:
                plt.show()

            return fig, ax

    def plotIVandCQE (self, showPlot=False):

        # TODO: get all axees, lines, etc from just fig
        figA, axA = self.plotIV(showEach=False)
        plt.close()

        if self.isDDS:
            figB, axB = self.plotCQE(showEach=False)
            plt.close()

        fig, (ax0, ax1) = plt.subplots(1,2)

        title = f'Sample:{self.sampleName}, PID:{self.samplePID}, Device:{self.name}'
        fig.suptitle (title)

        for line in axA.get_lines():
            #  plt.getp(line)
            ax0.semilogy(
                    line.get_xdata(),
                    line.get_ydata(),
                    linestyle=line.get_linestyle(),
                    marker=line.get_marker(),
                    label=line.get_label()
                )

        ax0.set_xlim(axA.get_xlim())
        ax0.set_ylim(axA.get_ylim())
        ax0.legend()

        if self.isDDS:
            for line in axB.get_lines():
                ax1.plot(
                        line.get_xdata(),
                        line.get_ydata(),
                        linestyle=line.get_linestyle(),
                        marker=line.get_marker(),
                        label=line.get_label()
                    )

            ax1.set_xlim(axB.get_xlim())
            ax1.set_ylim(axB.get_ylim())
            ax1.legend()

        if showPlot:
            fig.show()

        return fig

def jumpDetect (y, plot=False):

    yOrig = copy.deepcopy(y)

    #  y = y[y>0.05]
    y -= np.average(y)

    step = np.hstack((np.ones(len(y)), -1*np.ones(len(y))))

    y_step = np.convolve(y, step, mode='valid')

    # get the peak of the convolution, its index
    step_indx = np.argmax(y_step)
    print(step_indx)

    # plots
    if plot:

        plt.semilogy(yOrig, '+')
        plt.semilogy(y, 'x')

        plt.semilogy(step, '-.')
        plt.semilogy(y_step, ':')

        plt.semilogy((step_indx, step_indx), (y_step[step_indx]/10, 0), '--')

        plt.semilogy(yOrig[step_indx], 'or')
        plt.grid()

        plt.show()

def jumpRemove (y, yJump, yForw=2):
    '''
    Remove offset (step) of a curve
    '''

    #  TODO: make function to find jumps in y, and use it here to remove all them
    #  jumpDetect(y, plot=True)

    #  yOrig = copy.deepcopy(y)

    # Find the increment of the last three points
    inc = np.zeros(5)
    if not np.isnan(y).any() and len(y) > 1:
        inc[0] = y [y<yJump][-1] - y [y<yJump][-2]
        inc[1] = y [y<yJump][-2] - y [y<yJump][-3]
        inc[2] = y [y<yJump][-3] - y [y<yJump][-4]
        inc[3] = y [y<yJump][-4] - y [y<yJump][-5]
        inc[4] = y [y<yJump][-5] - y [y<yJump][-6]
        inc = np.average(inc, weights=[1,1,1,0,0])

        # Find offset between point before and yForw points after the jump/step
        if len(y[y>yJump]) > yForw:
            offset = y [y>yJump][yForw] - y [y<yJump][-1]
            offset = offset / (yForw + 1)

            # Remove offset and apply original increment to points above jump
            if offset > inc:
                y [y>yJump] = y [y>yJump] - offset + inc

    return y


def discardManual (deviceList, case):
    for dev in deviceList:
        dev.discard = True \
            if case == 'ContactDistance' and \
               (dev.sampleName == 'Aalto3.1' and dev.samplePID=='S125' and dev.name== '500H4') or\
               (dev.sampleName == 'Aalto3.1' and dev.samplePID=='S126' and dev.name== '250D4') or\
               (dev.sampleName == 'Aalto3.1' and dev.samplePID=='S126' and dev.name== '250D5') or\
               (dev.sampleName == 'Aalto3.1' and dev.samplePID=='S126' and dev.name== '250D2') or\
               (dev.sampleName == 'Aalto3.1' and dev.samplePID=='S126' and dev.name== '250A0') or\
               (dev.sampleName == 'Aalto3.1' and dev.samplePID=='S126' and dev.name== '1000B6') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S129' and dev.name== '250E7') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S129' and dev.name== '250E8') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S129' and dev.name== '250E9') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S129' and dev.name== '250F8') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S129' and dev.name== '250F9') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S134' and dev.name=='1000C8') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S134' and dev.name=='500E6P') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S135' and dev.name=='500K2P') or\
               (dev.sampleName == 'TPX0342'  and dev.samplePID=='S130' and dev.name=='250A9') or\
               (dev.sampleName == 'TPX0342'  and dev.samplePID=='S130' and dev.name=='1000A5') or\
               (dev.sampleName == 'TPX0342'  and dev.samplePID=='S130' and dev.name=='1000E0') or\
               (dev.sampleName == 'TPX0341'  and dev.samplePID=='S129' and re.search('250[0-9]', dev.name)) or\
               (dev.sampleName == 'TPX0342'  and dev.samplePID=='S130' and re.search('250[0-9]', dev.name))\
               else dev.discard

        # A marker to determine if a device was automatically or manually discarded
        dev.discardManual = True if dev.discard != dev.discardAuto else False

def createDF (deviceList, showDF=False, showTable=False, saveTable=False, saveDir='./output'):

    rows = []
    rowsUnits = []

    for dev in deviceList:
        rows.append(dev.__dict__)

    df = pd.DataFrame(rows)

    # TODO: create a pint-units-aware dataframe
    #  for key, value in dev.__dict__.items():
    #      if type(value) == pint.Quantity:
    #          rowsUnits.append(value.magnitude)
    #      else:
    #          rowsUnits.append('None')
    #  df = pd.DataFrame(rowsUnits)

    if showDF:
        print(df)

    # Create a table
    if showTable or saveTable:
        table= tabulate(df, headers='keys', tablefmt='github')
        #  table = tabulate(
        #          df.sort_values(['CQEm','isPassivated', 'Diameter', 'Device']),
        #          headers='keys',
        #          tablefmt='simple'
        #      )

    # Print to stdout
    if showTable:
        print(table)

    # ... and to a file
    if saveTable:
        f = open(saveDir+'/summaryTable.txt', 'w')
        f.write(table)
        f.close()

    return df

######################
# Curves manipulation
######################
def smooth(y, box_pts):
    '''
    smooth a curve
    '''

    box = np.ones(box_pts)/box_pts
    ySmooth = np.convolve(y, box, mode='valid')
    ySmooth = np.append(ySmooth, np.zeros(abs(len(y)-len(ySmooth))))
    return ySmooth

def fitR (voltage, resistance, conductance, smooth_width):
    #TODO define the fit
    '''
    Fit the resistance or conductance linear part to a polinomial, NOT WORKING
    '''
    #  for ui, resi, condi in zip(voltage, resistance, cond):
        #  print(us, '\n', res)
    voltage1 = np.linspace(0,10,smooth_width)
    norm = np.sum(np.exp(-voltage1**2)) * (voltage1[1]-voltage1[0]) # ad hoc normalization
    r1 = (4*voltage1**2 - 2) * np.exp(-voltage1**2) / smooth_width *8#norm*(x1[1]-x1[0])

    # calculate second order deriv.
    rConv = np.convolve(resistance, r1, mode="same")

    #  plt.plot(voltage1, rConv,  label='second deriv')
    plt.plot(voltage[:-1], resistance, label='resistance')
    plt.xlim(0.5, 3)
    plt.ylim(0, 10)
    plt.legend()
    #  plt.show()
    #  rsFit = np.polyfit (voltage[36:38], resistance[36:38], 1) # polynomial fit
    #  print(rsFit)

    #  for j, k in zip(voltage, resistance):
    #      print(j, '\t', k)


    rsFit = np.polyfit (voltage[0, 36:38], resistance[0, 36:38], 1) # polynomial fit
    rsFitFun = np.poly1d (rsFit)

    return rsFit, rsFitFun

####################
# Devices selection
####################
def selDevs1var (df, outVar, maxVar, *args):
    #  dg = df.set_index([out for out in outVar])\
    #          .groupby(args, sort=False)\
    #          .agg({maxVar:['max', 'idxmax']})
    df1 = df[df.groupby(args, sort=False)[maxVar].transform('max').eq(df[maxVar])]
    cols = [arg for arg in args] + [val for val in outVar] + [maxVar]
    dg = df1.loc[df1, cols]
    return dg

def selDevs2vars (df, outVar, maxVar, minVar, groupVars, showDF=False):
    '''
    Select the best devices in a dataFrame which have the maximum 'maxVar'
    and minimum 'minVar' with their corresponding outVar, for each group
    of values of args
    '''

    df1 = df[df.groupby(groupVars, sort=False)[maxVar].transform('max').eq(df[maxVar])]
    cols = [var for var in groupVars] + [val for val in outVar] + [maxVar, minVar]
    #  mask = df1.groupby(groupVars, sort=False)[minVar].idxmin()
    mask = df1.groupby(groupVars, sort=False)[minVar].transform('max').eq(df1[minVar])
    dg = df1.loc[mask, cols]

    if showDF:
        print(dg)

    return dg

def df2dict (df, maingroup, subgroup, selectedCol):

    selDict = {k: f.groupby(subgroup)[selectedCol].apply(list).to_dict()
     for k, f in df.groupby(maingroup)}

    return selDict

###########
# Plotting
###########
def mapcolor(colormap, Ncolors):
    '''
    create a custom colormap from certain matplolib colormap
    # Have a look at the colormaps here and decide which one you like:
    # http://matplotlib.org/1.2.1/examples/pylab_examples/show_colormaps.html
    '''
    Ncolors = min(colormap.N,Ncolors)
    mapcolors = [colormap(int(x*colormap.N/Ncolors)) for x in range(Ncolors)]
    return mapcolors

def availableMarkers(chooseAll=False):
    '''
    '''
    if chooseAll:
        markers = list(mpl.lines.lineMarkers.keys())
        #  markers = list(map(str, markers)) # convert all markers to strings
    else:
        markers = ['o','*','^','v','<','x','>','p','s','h','H','D','d', 'p','.','']
    return markers

def plot2vars (deviceList, selDevs, xVars, yVars, xLims, yLims, colorVar, colormap=plt.cm.Spectral):
    # TODO: add a way to set conditions, e.g. by a dictionary
    #  conds = { cqe: '<1', ...}
    # TODO: Make this function much more general, for any kind of analysis
    colorVarVals = list(selDevs[colorVar].unique())
    colors = mapcolor(colormap, len(colorVarVals))
    #  lines = iter (1000*['-','--','-.',':'])
    #  markers = iter(1000*availableMarkers (chooseAll=True))
    for diam in selDevs.diameter.unique():
        fig, ax = plt.subplots(1,2)
        fig.suptitle ('Diameter: {dia:.1e}'.format(dia=diam))
        for dev in deviceList:
            #  lineSty = next (lines)
            #  markerSty = next (markers)
            if dev.diameter == diam and \
                    not dev.discard and \
                    not selDevs.loc[(selDevs.sampleName==dev.sampleName) &
                            (selDevs.samplePID == dev.samplePID) &
                            (selDevs.name == dev.name)].empty:
                        ax[0].plot(getattr(dev, xVars[0]), getattr(dev, yVars[0]),
                                c = colors[colorVarVals.index(getattr(dev,colorVar))],
                                #  linestyle = lineSty, marker = markerSty,
                                linestyle = dev.lineSty, marker = dev.markerSty,
                                label='S:{smp}|P:{pid}|D:{dev}'
                                   .format(smp=dev.sampleName, pid=dev.samplePID,dev=dev.name))
                        ax[0].set_xlabel(xVars[0])
                        ax[0].set_ylabel(yVars[0])
                        ax[0].set_xlim(xLims[0])
                        ax[0].set_ylim(yLims[0])
                        ax[0].legend(ncol=2, loc='best')
                        ax[1].plot(getattr(dev,xVars[1]), getattr(dev,yVars[1]),
                                c=colors[colorVarVals.index(getattr(dev,colorVar))],
                                #  linestyle = dev.lineSty,
                                linestyle = '-',
                                marker = dev.markerSty,
                                label='S:{smp}|P:{pid}|D:{dev}'
                                   .format(smp=dev.sampleName, pid=dev.samplePID,dev=dev.name))
                        ax[1].set_xlabel(xVars[1])
                        ax[1].set_ylabel(yVars[1])
                        ax[1].set_xlim(xLims[1])
                        ax[1].set_ylim(yLims[1])
                        ax[1].legend(ncol=2, loc='best')

def plotStatistics(dataFrame, x, y, hue, columnsData, title='', axisLabels = None, axisLimits = None, axisTicks = None, markerSize = 60, col_wrap = 2, palette = 'Set1', adjustSubplots = True, differentHueMarkers = False, legend_out = True, fitReg = False, savePlot=False, chooseAllAvailableMarkers = False):
    '''
    '''
    # TODO: get it to work without hue or columnsData = None

    if chooseAllAvailableMarkers:
        m_styles = availableMarkers(chooseAll=True)
    else:
        m_styles = availableMarkers(chooseAll=False)

    m_array = m_styles[0:dataFrame[hue].nunique()]

    if differentHueMarkers:
        markerSty = m_array
    else:
        markerSty = 'o'

    g = sns.lmplot (x, y,
            data = dataFrame,
            hue = hue,
            #  fit_reg = fitReg,
            col = columnsData,
            #  col_wrap = col_wrap,
            #  palette = palette,
            #  markers = markerSty,
            #  scatter_kws = {'s': markerSize},
            #  legend_out = legend_out,
            )

    g.fig.suptitle (title)

    if axisLabels:
        xLab, yLab = axisLabels
        g.set_axis_labels (xLab, yLab)

    if axisLimits:
        xLim, yLim = axisLimits
        g.set (xlim = xLim, ylim = yLim), #axisLimis = [(0, 60), (0, 12)]

    if axisTicks:
        xTicks, yTicks = axisTicks
        g.set (xticks=xTicks, yticks=yTicks)

    if adjustSubplots:
        g.fig.subplots_adjust (wspace = .02, hspace = .05)

    if savePlot:
        plt.savefig(savePlot)
        print('Figured saved in {}'.format(savePlot))

def plot3d (title, dataFrame, xParam, yParam, zParam):
    fig = plt.figure()
    plt.title(title)
    #-----------------------------------------------------------
    ax = fig.add_subplot(221, projection='3d')
    dg = dataFrame.query('Diameter == 0.000250 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    #  z = (z-np.nanmin(z)) / (np.nanmax(z) - np.nanmin(z))
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 250 $\mu$m')
    ax.grid(True)
    #-----------------------------------------------------------
    ax = fig.add_subplot(222, projection='3d')
    dg = dataFrame.query('Diameter == 0.000500 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 500 $\mu$m')
    ax.grid(True)
    #-----------------------------------------------------------
    ax = fig.add_subplot(223, projection='3d')
    dg = dataFrame.query('Diameter == 0.001000 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 1000 $\mu$m')
    ax.grid(True)
    #-----------------------------------------------------------
    ax = fig.add_subplot(224, projection='3d')
    dg = dataFrame.query('Diameter == 0.002000 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 2000 $\mu$m')
    ax.grid(True)
