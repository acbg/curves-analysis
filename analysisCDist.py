#############################
# Analysis: Contact distance
#############################
import time as tm
st = tm.time() # Start counting time

#---------------------------------------------------------------------------------------
print(f"*** Elapsed time: {tm.time() - st:.2f} s (Analysis: Contact distance)")
#---------------------------------------------------------------------------------------

import re
import sys
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from scipy.interpolate import make_interp_spline, BSpline

pd.set_option('max_rows', None)

import funsAnalysis as f

#####################
# Physical constants
#####################

# access to all other packages in the upper directory
sys.path.append('./')
sys.path.append('../')
from physics.constants import ureg, Q_
ureg.setup_matplotlib()
#  import pintpandas
import physics.constants as pc
import physics.functions as pf

#  Matplotlib settings
#  --------------------
#  mpl.rcParams.update(mpl.rcParamsDefault)

#  print(plt.style.available)

plt.rcParams.update(plt.rcParamsDefault)

#  plt.style.use('dark_background')
#  plt.style.use('seaborn-dark')
plt.style.use('ggplot')

plt.rcParams.update({
    'text.usetex'       : True,
    #  'axes.edgecolor'    : 'dimgrey',
    'axes.grid'         : False,
    'axes.facecolor'    : 'grey',
    'font.size'         : 5.0,
    'lines.linewidth'   : 1.0,
    'lines.markersize'  : 1.0,
    'legend.shadow'     : False,
    'legend.fancybox'   : False,
    'legend.fontsize'   : 5.5,
    #  'font.family': 'serif',
    #  'xtick.labelsize' : 17,
    #  'ytick.labelsize' : 17,
    'savefig.format'     : 'pdf',
    })


# #########
# Functions
# #########

def plotCDistRaw (
        devList, selDevs,
        xVars, yVars,
        xLims=[[],[]], yLims=[[],[]],
        xLbls=[[],[]], yLbls=[[],[]],
        colorVar=None, linestyVar=None, figVar=None, paramVar=None,
        lineStys=['-','-'], paramVars=[],
        colormap=plt.cm.Greys,
        deviceNameInLeg=False,
        title='No title set', saveDir='./', savePrefix='', saveExt='jpg'
    ):
    '''
    Arguments

    devList: a list containing Device class objects
    selDevs: a dataframe with the selected devices to be plotted

    xVars, yVars: arrays with the variables to be plotted
    xLims, yLims: arrays containing the limits for each variable;
                    if unset, the limits are the maximum and minimum value of the plotted variables

    These variable will determine different aspects of the plotting:

        colorVar: curve color is assigned to different values of this variable
        linestyVar: curve linestyle is assigned according to this variable
            lineStys: the linestyles that will be used
        paramVar: a different subplot for each value of this variable
            paramVars: if one needs to use only selected values of this variable
        figVar: a new figure for each value

    deviceNameInLeg: will show the sampleName and device name in the legend
    '''

    colors = f.mapcolor(colormap, len(list(selDevs[colorVar].unique())))
    colorVarVals = list(selDevs[colorVar].unique())
    colorVarVals.sort() #order the values

    if linestyVar:
        lineStysVals = list(selDevs[linestyVar].unique())

    # If paramVars is not set when calling the function,
    # the number of coulmns will be the total number
    # of different values of parmVar in selDevs df:
    if len(paramVars) == 0:
        paramVars = list(selDevs[paramVar].unique())

    # Number of subplot rows and columns:
    nrows = len(xVars)
    ncols = len(paramVars)

    # Plot diferent values of figVar in different figures
    for figV in list(selDevs[figVar].unique()):
        fig, ax = plt.subplots(nrows, ncols)
        fig.suptitle(figV+' ['+title+']')

        for dev in devList:

            # Conditions for plotting each device
            if dev.isDDS and \
                getattr(dev,figVar) == figV and \
                not selDevs.loc[(selDevs.sampleName==dev.sampleName) &
                        (selDevs.samplePID == dev.samplePID) &
                        (selDevs.name == dev.name)].empty:

                #  assign axis to diameters:
                #  for ii, diam in enumerate(list(selDevs['diameter'].unique())):
                for ii, param in enumerate(sorted(paramVars)):

                    if len(paramVars) < 2:
                        ax0, ax1 = ax[0], ax[1]
                    else:
                        ax0, ax1 = ax[0,ii], ax[1,ii]

                    # and plot each diameter in one axis:
                    if getattr(dev,paramVar) == param:

                        # Define legend labels
                        # parVar:9.0f: the number is set so that strings are
                        # well sorted out when they are numbers, since the
                        # initial spacings help the sorting.
                        lbl = f'{dev.sampleName: ^s}{dev.samplePID: ^s}{dev.name: ^s}: ${getattr(dev,colorVar): ^9.0f~L} | {getattr(dev,linestyVar): ^9.0f~L} | {dev.Rs.to(ureg.ohm): ^0.1f~L}$' \
                            if deviceNameInLeg else \
                            f'${getattr(dev,colorVar): 9.0f~L} | {getattr(dev,linestyVar): 9.0f~L} | {dev.Rs.to(ureg.ohm):0.1f~L}$'

                        ax0.semilogy (
                                getattr(dev, xVars[0]), abs(getattr(dev, yVars[0])),
                                c=colors[colorVarVals.index(getattr(dev,colorVar))],
                                #  marker = 'o',
                                ls = lineStys[0] \
                                    if getattr(dev,colorVar) == getattr(dev,linestyVar) \
                                    else lineStys[1],
                                label = lbl,
                            )

                        # Continuous measurements
                        #  ax0.semilogy (
                        #          dev.uc, dev.iLc,
                        #          c=colors[colorVarVals.index(getattr(dev,colorVar))],
                        #          ls = '--',
                        #      )


                        if len(yVars)>2 and yVars[2] == 'iD':
                            ax0.semilogy(
                                    getattr(dev, xVars[0]), 0.1*abs(getattr(dev, yVars[2])),
                                    c=colors[colorVarVals.index(getattr(dev,colorVar))],
                                    ls = '--',
                                    #  label = lbl,
                                )

                        ax1.plot (
                                getattr (dev, xVars[1]), abs (getattr (dev, yVars[1])),
                                c = colors[colorVarVals.index (getattr (dev,colorVar))],
                                #  marker='o', markersize=4,
                                ls = lineStys[0] \
                                    if getattr(dev,colorVar) == getattr(dev,linestyVar) \
                                    else lineStys[1],
                            )

                        # PCE
                        #  ax1.plot (
                        #          getattr (dev, xVars[1]), dev.pce.magnitude,
                        #          c = colors[colorVarVals.index (getattr (dev,colorVar))],
                        #          ls = '--'
                        #      )

                        # Continuous measurements
                        #  ax1.plot (
                        #          dev.uc, dev.cqec,
                        #          c=colors[colorVarVals.index(getattr(dev,colorVar))],
                        #          ls = '--',
                        #      )

                        #--------------
                        # Hide y axes from all subplots but the most left ones
                        if getattr(dev,paramVar) != min(list(selDevs[paramVar].unique())):
                            ax0.yaxis.set_visible(False)
                            ax1.yaxis.set_visible(False)
                        # Hide x axes from the first subplots row
                        ax0.xaxis.set_visible(True)
                        ax0.set_xlabel(xLbls[0])
                        ax0.set_ylabel(yLbls[0])
                        # Set the limits to the minimum and maximum of the x and y variables:
                        if len(xLims[0]) == 0:
                            ax0.set_xlim([min(getattr(dev,xVars[0])), max(getattr(dev,xVars[0]))])
                        else:
                            ax0.set_xlim(xLims[0])
                        if len(yLims[0]) == 0:
                            ax0.set_ylim([min(getattr(dev,yVars[0])), max(getattr(dev,yVars[0]))])
                        else:
                            ax0.set_ylim(yLims[0])

                        # Subplots titles
                        # ---------------
                        ax0.set_title(f'${paramVar:s} = {getattr(dev,paramVar):.0f~L}$')

                        # Legend
                        # ------
                        ax0.legend()
                        # Sort legend labels alphabetically by name
                        handles, labels = ax0.get_legend_handles_labels()
                        labels, handles = zip(*sorted(zip(labels, handles), key=lambda t: t[0]))
                        #  labels, handles = zip(*sorted(zip(labels, handles), \
                        #          key=lambda item: (int(item.partition(' ')[0])
                        #         if item[0].isdigit() else float('inf'))))

                        legend_title = f'Samp - PID - Device: {colorVar:^5s} | {linestyVar:^5s} | Rs'\
                            if deviceNameInLeg \
                            else f'{colorVar:^5s} | {linestyVar:^5s} | {paramVar:^5s}'

                        ax0.legend(
                                handles, labels,
                                ncol=1,
                                loc='lower right',
                                title = legend_title,
                                columnspacing=1.0, labelspacing=0.0,
                                handletextpad=0.0, handlelength=1.5,
                                #  prop={'size':6},
                            )

                        #--------------
                        ax1.set_xlabel(xLbls[1])
                        ax1.set_ylabel(yLbls[1])
                        if len(xLims[1]) == 0:
                            ax1.set_xlim([min(getattr(dev,xVars[1])), max(getattr(dev,xVars[1]))])
                        else:
                            ax1.set_xlim(xLims[1])
                        if len(yLims[1]) == 0:
                            ax1.set_ylim([min(getattr(dev,yVars[1])), max(getattr(dev,yVars[1]))])
                        else:
                            ax1.set_ylim(yLims[1])



        if saveDir:
            filename = title.replace('|','').replace('(','').replace(')','').replace(' ','_')
            filename='-'.join([savePrefix,figV,filename])
            filename = '.'.join([filename,saveExt])
            output = '/'.join([saveDir,filename])
            fig.savefig(output, dpi=200)

        fig.set_dpi(130)

def plotCDistMax (
        devList, selDevs,
        x = None, y = None,
        xName = None, yName = None,
        xLims = None, yLims = None,
        figV = None,
        rowV = None, colV = None,
        paramV = None,  paramVars = [],
        hueV = None,
        lineV = None, lineStys = [],
        colormap = plt.cm.Spectral,
        title = 'No title set',
        saveFig = False, saveDir = './', savePrefix = '', saveExt = 'eps'
    ):

    if lineV:
        lineVals = list(selDevs[lineV].unique())

    # If paramVars is not set when calling the function,
    # the number of coulmns will be the total number
    # of different values of parmVar in selDevs df:
    #  if len(paramVars) == 0:
    #      paramVs = list(selDevs[paramV].unique())

    # Values of the column, row and color variables
    rowVals = list (selDevs[rowV].unique())
    colVals = list (selDevs[colV].unique())

    # Order values
    rowVals.sort() #order the values
    colVals.sort() #order the values

    nrows = len (rowVals)
    ncols = len (colVals)

    #  # Colors to be used
    #  hueVals = list (selDevs[hueV].unique())
    #  hueVals.sort() #order the values
    #  nhue = len (hueVals)
    #
    #  colors = f.mapcolor(colormap, nhue)


    # Create the arrays with the desired values to be plotted
    xVals = np.array (selDevs[x].unique())

    # initialize them as zeros arrays
    xs  = np.zeros ([nrows, ncols, len(xVals)])
    ys1 = np.zeros ([nrows, ncols, len(xVals)])
    ys2 = np.zeros ([nrows, ncols, len(xVals)])

    # Fill with NaNs so that inexistent values are not plotted
    xs.fill(np.nan)
    ys1.fill(np.nan)
    ys2.fill(np.nan)



    for r, row in enumerate(rowVals):
        for c, col in enumerate(colVals[:-1]):
        #  for c, col in enumerate(colVals):
            for j, xi in enumerate(xVals):
                for dev in devList:
                    if getattr(dev, rowV) == row and \
                        getattr (dev, colV) == col and \
                        getattr (dev, x) == xi and \
                        not dev.discard and \
                        not selDevs.loc[
                            (selDevs.sampleName == dev.sampleName) &
                            (selDevs.samplePID == dev.samplePID) &
                            (selDevs.name == dev.name)].empty:

                        xs[r, c, j] = getattr (dev, x).magnitude
                        #  xs[r, c, j] = getattr (dev, 'd2perim').magnitude

                        #  print(dev.sampleName, dev.samplePID,dev.name, getattr(dev,y), dev.cqeM)
                        if getattr (dev, lineV) == getattr (dev, x):
                            ys1[r, c, j] = getattr(dev, y).magnitude
                        else:
                            ys2[r, c, j] = getattr(dev, y).magnitude


    # Sort the arrays
    # ---------------
    order = xs.argsort(axis=2)
    xs = np.take_along_axis(xs, order, 2)
    ys1 = np.take_along_axis(ys1, order, 2)
    ys2 = np.take_along_axis(ys2, order, 2)

    # Make the plots
    # ---------------
    fig, ax = plt.subplots(nrows, ncols-1)
    #  fig, ax = plt.subplots(nrows, ncols)
    fig.suptitle(title)

    for r, row in enumerate(rowVals):
        for c, col in enumerate(colVals[:-1]):
        #  for c, col in enumerate(colVals):

            #the plotting
            ax[r][c].plot(xs[r,c], ys1[r,c], 'o-')
            ax[r][c].plot(xs[r,c], ys2[r,c], 'x:')

            # columns titles
            ax[0][c].set_title(colVals[c])
            # rows titles
            ax[r][0].text(-0.4, 0.5, rowVals[r],
                    transform=ax[r][0].transAxes, rotation=90,
                    verticalalignment='center', horizontalalignment='center')

            ax[r][c].set_xlabel(xName)
            ax[r][c].set_ylabel(yName)
            # Hide y axes from all subplots but the most left ones
            if r != (len(rowVals) - 1):
                ax[r][c].xaxis.set_visible(False)
            # Hide x axes from the first subplots row
            if c != 0:
                ax[r][c].yaxis.set_visible(False)

            ax[r][c].set_xlim(xLims)
            ax[r][c].set_ylim(yLims)

            # Set grids
            #  ax[r][c].grid(axis='y')

    if saveFig:
        filename = title.replace('|','').replace('(','').replace(')','').replace(' ','_')
        filename='-'.join([savePrefix,filename]) if savePrefix else filename
        filename = '.'.join([filename,saveExt])
        output = '/'.join([saveDir,filename])
        fig.savefig(output, dpi=200)

    fig.set_dpi(250)

def plotCDistMaxDiffYCol (
        devList, selDevs,
        x=None,
        #  y=None,
        xName=None, yName=None,
        xLims=None, yLims=None,
        figV=None,
        rowV=None, colV=None,
        paramV=None, paramVars=[],
        hueV=None,
        lineV=None, lineStys=[],
        colormap=plt.cm.Spectral,
        title='No title set',
        saveFig=False, saveDir='./', savePrefix='', saveSufix='', saveExt='eps'
    ):

    if lineV:
        lineVals = list(selDevs[lineV].unique())

    # If paramVars is not set when calling the function,
    # the number of coulmns will be the total number
    # of different values of parmVar in selDevs df:
    #  if len(paramVars) == 0:
    #      paramVs = list(selDevs[paramV].unique())

    # Values of the column, row and color variables

    rowVals = list (selDevs[rowV].unique())
    rowVals.sort() #order the values
    nrows = len (rowVals)

    #  colVals = list (selDevs[colV].unique())
    #  colVals.sort() #order the values
    ncols = len (colV)

    hueVals = list (selDevs[hueV].unique())
    hueVals.sort() #order the values
    nhue = len (hueVals)


    # Create the arrays with the desired values to be plotted
    xVals = np.array (selDevs[x].unique())

    # initialize them as zeros arrays
    xs0 = np.zeros ([nrows, ncols, nhue, len(xVals)])
    xs1 = np.zeros ([nrows, ncols, nhue, len(xVals)])
    ys1 = np.zeros ([nrows, ncols, nhue, len(xVals)])
    ys2 = np.zeros ([nrows, ncols, nhue, len(xVals)])

    # Fill with NaNs so that inexistent values are not plotted
    xs0.fill (np.nan)
    xs1.fill(np.nan)
    ys1.fill(np.nan)
    ys2.fill(np.nan)



    for r, row in enumerate(rowVals):
        for c, col in enumerate(colV):
            for h, hue in enumerate(hueVals):
                for j, xi in enumerate(xVals):
                    for dev in devList:
                        if getattr(dev, rowV) == row and \
                            getattr (dev, hueV) == hue and \
                            getattr (dev, x) == xi and \
                            not dev.discard and \
                            not selDevs.loc[
                                (selDevs.sampleName == dev.sampleName) &
                                (selDevs.samplePID == dev.samplePID) &
                                (selDevs.name == dev.name)].empty:

                            xs0[r, c, h, j] = getattr (dev, 'dT2rad').magnitude
                            xs1[r, c, h, j] = getattr (dev, x).magnitude

                            if getattr (dev, lineV) == getattr (dev, x):
                                ys1[r, c, h, j] = getattr(dev, col).magnitude
                            else:
                                ys2[r, c, h, j] = getattr(dev, col).magnitude


    # Sort the arrays
    # ---------------
    order = xs0.argsort(axis=3)
    xs0 = np.take_along_axis(xs0, order, 3)
    xs1 = np.take_along_axis(xs1, order, 3)
    ys1 = np.take_along_axis(ys1, order, 3)
    ys2 = np.take_along_axis(ys2, order, 3)

    # Make the plots
    # ---------------
    fig, ax = plt.subplots(nrows, ncols)
    fig.suptitle(title)
    fig.subplots_adjust(wspace=0.4)

    colors = f.mapcolor(colormap, nhue)

    # choose the x variables
    xs = xs0
    xs = xs1

    for r, row in enumerate(rowVals):
        for c, col in enumerate(colV):
            for h, hue in enumerate(hueVals):

                x = xs[r,c,h]
                y1 = ys1[r,c,h]
                y2 = ys2[r,c,h]

                #the plotting
                ax[r][c].plot(x, y1, 'o-',c=colors[h], label=f'{hue}')
                ax[r][c].plot(x, y2, 'x:', c=colors[h])

                # splines
                #  print(x, y1)
                #  y1=y1.ravel()
                #  print(x, y1)
                #  y1=(y1[y1!=np.isnan])
                #  x=x.ravel()
                #  x=(x[y1!=np.isnan])
                #  print(x, y1)
                #  x_smooth = np.linspace(x.min(), x.max(), 300)
                #  spl = make_interp_spline(x, y1, k=3)
                #  y1_smooth = spl(x_smooth)
                #  ax[r][c].plot(x_smooth, y1_smooth, '-', c=colors[h])

                # columns titles
                #  ax[0][c].set_title(colV[c])
                # rows titles
                ax[r][0].text(-0.3, 0.5, rowVals[r],
                        transform=ax[r][0].transAxes, rotation=90,
                        verticalalignment='center', horizontalalignment='center')

                ax[r][c].set_xlabel(xName)
                ax[r][c].set_ylabel(yName[c])
                ax[r][c].legend()

                # Hide x axes from all subplots but the most left ones
                if r != (len(rowVals) - 1):
                    ax[r][c].xaxis.set_visible(False)

                # Hide y axes from the first subplots row
                #  if c != 0:
                #      ax[r][c].yaxis.set_visible(False)

                #  ax[r][c].set_xlim(xLims)
                #  ax[r][c].set_ylim(yLims)

                # Set grids
                #  ax[r][c].grid(axis='y')

    if saveFig:
        filename = title.replace('|','').replace('(','').replace(')','').replace(' ','_')
        filename='-'.join([savePrefix,filename]) if savePrefix else filename
        filename='-'.join([filename,saveSufix]) if saveSufix else filename
        filename = '.'.join([filename,saveExt])
        output = '/'.join([saveDir,filename])
        fig.savefig(output, dpi=200)

    fig.set_dpi(250)

# Create device list
#####################
dirList = [
        #  'G/Aalto3.1/S125/IVdata/',
        #  'G/Aalto3.1/S126/IVdata/',
        'TPX/TPX0300-TPX0399/TPX0341/S129/IVdata/',
        'TPX/TPX0300-TPX0399/TPX0341/S134/IVdata/',
        'TPX/TPX0300-TPX0399/TPX0341/S135/IVdata/',
        'TPX/TPX0300-TPX0399/TPX0342/S130/IVdata/',
        'TPX/TPX0300-TPX0399/TPX0342/S132/IVdata/',
    ]

devList, devsDF = f.createDevList(
        st,
        rootDir='/l/Samples/',
        case='ContactDistance',
        dirList=dirList,
        #  mask = ['500D1.dat','500E6.dat'],
        #  mask = ['500E6.dat'],
        printFileList=0, printDF=0,
    )


# ######################################################################
# Plots
print(f"*** Elapsed time: {tm.time() - st:.2f} s (Plot figures)")
# ######################################################################

colorMap = plt.cm.Spectral
colorMap = plt.cm.Oranges
colorMap = plt.cm.Greens

#Raw curves
'''
# To plot the 4pp devices, the bestdevsDF must be modified to account for cqem4pp
#  df1 = devsDF[devsDF.groupby(['sampleName', 'diameter', 'dT', 'dM'], sort=False)['cqeM4pp'].transform('max').eq(devsDF['cqeM4pp'])]
#  print(df1[['sampleName', 'samplePID', 'diameter', 'name', 'dT', 'dM', 'cqeM4pp']])
#  print(devsDF[devsDF.has4pp == True][['sampleName', 'samplePID', 'name', 'dT', 'dM', 'cqeM4pp']])
bestDevsDF = f.selDevs2vars (
        df=devsDF,
        outVar=['name', 'samplePID'],
        maxVar='cqeM4pp',
        minVar='cqeM4pp',
        #  groupVars=['sampleName', 'diameter', 'has4pp', 'dT', 'dM'],
        groupVars=['sampleName', 'diameter', 'has4pp', 'dT'],
        showDF=False,
    )

plotCDistRaw (
        devList,
        selDevs=bestDevsDF,
        title = 'Contact distance | 4pp',
        xVars = ['u4pp', 'u4pp'],
        xLims = [[0.0, 2.0], [1.0, 2.0]] * ureg.V,
        yVars = ['iL4pp', 'cqe4pp', 'iD4pp'],
        yLims = [[1e-9,5e-1] * ureg.A, [0.1,0.7]],
        colorVar = 'dT', colormap = colorMap,
        #  linestyVar = 'dM', lineStys = ['-', ':'],
        linestyVar = 'dT', lineStys = ['-', ':'],
        paramVar = 'diameter', paramVars = [1000] * ureg.micron,
        figVar = 'sampleName',
        deviceNameInLeg=True,
        saveDir = 'output', savePrefix = 'exp', saveExt = 'jpg'
    )
#  '''

'''
bestDevsDF = f.selDevs2vars (
        df=devsDF,
        outVar=['name', 'samplePID', 'dM'],
        maxVar='cqeM', minVar='cqeM',
        groupVars=['sampleName', 'diameter', 'dT'],
        showDF=False,
    )

plotCDistRaw (
        devList,
        title = 'Diameter effect | 2pp',
        selDevs=bestDevsDF,
        xVars = ['u', 'u'],
        xLims = [[0.0, 4.5], [1.0, 3.5]] * ureg.V,
        yVars = ['iL', 'cqe'],
        yLims = [[1e-13,5e-1] * ureg.A, [0.1,0.7]],
        colorVar = 'diameter', colormap = colorMap,
        paramVar = 'dT',# paramVars = [0.00025, 0.0005, 0.001],
        linestyVar = 'dM', #lineStys = ['-', ':'],
        figVar = 'sampleName',
        saveDir = 'output', savePrefix = 'exp', saveExt = 'jpg'
    )
#  '''

#  '''
bestDevsDF = f.selDevs2vars (
                df = devsDF[(devsDF['cqeM']<1) & (devsDF['discard'] == False)],
                outVar = ['name', 'samplePID', 'cqeMu'],
                maxVar = 'cqeM',
                minVar = 'Rs',
                #  groupVars = ['sampleName', 'diameter', 'dT', 'dM'],
                groupVars = ['sampleName', 'diameter', 'dT', 'dM'],
                showDF = 0,
            )

#  '''
plotCDistRaw (
        devList,
        title = 'Contact distance | 2pp',
        selDevs=bestDevsDF,
        xVars = ['u', 'u'],
        xLbls = ['$U$', '$U$'],
        #  xLbls = ['$U_L = 1.2 + kT/q * log(I_D)$', '$U_L$'],
        #  xLims = [[0.4, 2.5] * ureg.V, [1.0, 2.5] * ureg.V],
        yVars = ['iL', 'cqe', 'iD'],
        #  yVars = ['jL', 'cqe', 'jD'],
        yLbls = ['$I_L$ and $0.1 I_D$', '$CQE$'],
        yLims = [[1e-9,1e-0] * ureg.A, [0.0,1.0]],
        #  yLims = [[1e-15,1e-4] * ureg.A / ureg.micron**2, [0.0,1.0]],
        colorVar = 'dT', colormap=colorMap,
        #  linestyVar = 'dT', lineStys = ['-', ':'],
        linestyVar = 'dM', lineStys = ['-', ':'],
        paramVar = 'diameter', paramVars = [250, 500, 1000]*ureg.micron,
        deviceNameInLeg= True,
        figVar = 'sampleName',
        saveDir = 'output', savePrefix = 'exp-dT-dM', saveExt = 'pdf'
    )
#  '''

# Maximum values
# --------------
plotCDistMaxDiffYCol (
        devList, bestDevsDF,
        x = 'dT', xName = '$d_T$',
        #  xLims = [0, 100]*ureg.micron,
        yLims = [0.4, 0.7]*ureg.ohm,
        #  colV = ['cqeM', 'cqeMuL', 'cqeG', 'cqeGuL', 'Rs'],
        #  yName = [f'$CQE_M$', f'$U_L (CQE_M)$', f'$CQE_G$', f'$U_L (CQE_G)$', f'$R_S$'],
        colV = ['cqeM', 'chi'],
        yName = [f'$CQE_M$', f'$\chi$'],
        rowV = 'sampleName',
        hueV = 'diameter',
        lineV = 'dM',
        colormap = plt.cm.Greens,
        title = 'chi',
        saveFig = True, saveDir = './output', saveSufix = 'dT', saveExt = 'pdf',
        )

'''
plotCDistMax (
        devList, bestDevsDF,
        x = 'dT', xName = '$d_T$',
        #  xLims = [0, 100]*ureg.micron,
        y = 'cqeM', yName = f'$CQE_M$',
        yLims = [0.4, 0.7]*ureg.ohm,
        colV = 'diameter',
        rowV = 'sampleName',
        hueV = None,
        lineV = 'dM',
        title = 'Maximum CQE',
        saveFig = True, saveDir = './output', savePrefix = 'dT2perim', saveExt = 'pdf',
        )

plotCDistMax (
        devList, bestDevsDF,
        x = 'dT', xName = f'$d_T$',
        #  xLims = [0, 100]*ureg.micron,
        y = 'Rs', yName = f'$R_S$',
        yLims = [0.0, 25]*ureg.ohm,
        colV = 'diameter',
        rowV = 'sampleName',
        hueV = None,
        lineV = 'dM',
        title = 'Series Resistance',
        saveFig = True, saveDir = './output', savePrefix = '', saveExt = 'pdf',
        )

plotCDistMax (
        devList, bestDevsDF,
        x = 'dT', xName = f'$d_T$',
        #  xLims = [0, 100]*ureg.micron,
        y = 'cqeMu', yName = f'$U(CQE_M)$',
        yLims = [1.0, 4.5]*ureg.volt,
        colV = 'diameter',
        rowV = 'sampleName',
        hueV = None,
        lineV = 'dM',
        title = 'Maximum CQE Voltage',
        saveFig = True, saveDir = './output', savePrefix = '', saveExt = 'pdf',
        )

plotCDistMax (
        devList, bestDevsDF,
        x = 'dT', xName = f'$d_T$',
        #  xLims = [0, 100]*ureg.micron,
        y = 'cqeMi', yName = f'$I(CQE_M)$',
        #  yLims = [0.0, 40]*ureg.ohm,
        colV = 'diameter',
        rowV = 'sampleName',
        hueV = None,
        lineV = 'dM',
        title = 'Maximum CQE Current',
        saveFig = True, saveDir = './output', savePrefix = '', saveExt = 'pdf',
        )

plotCDistMax (
        devList, bestDevsDF,
        x = 'dT', xName = f'$d_T$',
        #  xLims = [0, 100]*ureg.micron,
        y = 'cqeG', yName = f'$CQE_G$',
        #  yLims = [0.0, 40]*ureg.ohm,
        colV = 'diameter',
        rowV = 'sampleName',
        hueV = None,
        lineV = 'dM',
        title = 'CQE at Gap bias',
        )
'''

plt.show()
#  '''

'''
# Plot based on DataFrame
# ----------
bestDevsDF.dT = pd.Series([x.to_base_units().magnitude for x in bestDevsDF.dT.values], dtype="pint[micron]")
print(np.array([dT.to_base_units().magnitude for dT in bestDevsDF.dT.values]), bestDevsDF.dT.values[0].units)
#  bestDevsDF.dM = pd.Series(bestDevsDF.dM.values, dtype="pint[micron]")
#  bestDevsDF.diameter = pd.Series(bestDevsDF.diameter.values, dtype="pint[micron]")
#  bestDevsDF.Rs = pd.Series(bestDevsDF.Rs.values, dtype="pint[ohm]")
#  bestDevsDF.cqeMu = pd.Series(bestDevsDF.cqeMu.values, dtype="pint[volt]")
print(bestDevsDF.dtypes)
print(bestDevsDF)

g = sns.swarmplot( x='dT', y='cqeM', data=bestDevsDF)
plt.show()

g = sns.FacetGrid (bestDevsDF, hue='diameter', col='sampleName', margin_titles=True)
f = g.map (sns.pointplot, 'dT', 'Rs')
plt.show()
f = g.map (sns.pointplot, 'dT', 'Rs')
plt.show()

g = sns.lmplot (
        'dT', 'cqeM',
        data = bestDevsDF,
        #  hue = 'sampleName',
        #  fit_reg = False,
        #  col = 'diameter',
        #  col_wrap = col_wrap,
        #  palette = palette,
        #  markers = markerSty,
        #  scatter_kws = {'s': markerSize},
        #  legend_out = legend_out,
        )
#  '''

plt.show()

#---------------------------------------------------------------------------------------
print(f"*** Elapsed time: {tm.time() - st:.2f} s (End of analysis)")
#---------------------------------------------------------------------------------------
