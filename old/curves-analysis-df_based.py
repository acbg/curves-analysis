import time # for elapsed time evaluation
# Track time
st=time.time() # count time from here
print("*** Elapsed time: %.2f s (Initialize/import modules)"%(time.time()-st))
import os
import glob
import pandas as pd
#  pd.set_option("display.max_rows",10)
#  from pandas.tools.plotting import andrews_curves
import numpy as np
import matplotlib.pyplot as plt
import re # regular expressions for strings


# Create DataFrame
# ################

# Read files
# ----------
print("*** Elapsed time: %.2f s (Load files)"%(time.time()-st))

initial_path = os.getcwd() # read initial directory
os.chdir('../../../../ZIM/notes/iTPX/Samples/') # change directory to the root of samples
path = r'G/Aalto3.1/Aalto3.1-S12[5,6]/IVdata/' # select which samples you want
filelist = glob.glob(os.path.join(path, "*.dat"))

# Define objects
df = pd.DataFrame()
# ------------------------------------------------------
u = np.array(np.random.rand(len(filelist)),dtype=object)
uD = np.array(np.random.rand(len(filelist)),dtype=object)
iL = np.array(np.random.rand(len(filelist)),dtype=object)
iD = np.array(np.random.rand(len(filelist)),dtype=object)
uc = np.array(np.random.rand(len(filelist)),dtype=object)
uDc = np.array(np.random.rand(len(filelist)),dtype=object)
iLc = np.array(np.random.rand(len(filelist)),dtype=object)
iDc = np.array(np.random.rand(len(filelist)),dtype=object)
for ii in range(len(filelist)):
    #  print('file:', filelist[ii])

    '''
    # Store observables as arrays
    # ---------------------------
    # Create DataFrame cells as arrays from csv columns
    df.loc[ii,'iL']  = file_df[ 'IS_pulse'].values
    df.loc[ii,'iD']  = file_df[' ID_pulse'].values
    df.loc[ii,'u']   = file_df[' VS_pulse'].values
    df.loc[ii,'uD']  = file_df[' VD_pulse'].values
    df.loc[ii,'iLc'] = file_df[' IS_cont' ].values
    df.loc[ii,'iDc'] = file_df[' ID_cont' ].values
    df.loc[ii,'uc']  = file_df[' VS_cont' ].values
    df.loc[ii,'uDc'] = file_df[' VD_cont' ].values
    #  '''

    #  Read each csv file and add the IV data
    headers = ['iL', 'u', 'iD', 'uD', 'iLc', 'uc', 'iDc', 'uDc']
    file_df = pd.read_csv(filelist[ii], delimiter=',', comment='#', skiprows=22, names=headers)

    '''
    # Store data in separated numpy ndarrays
    iL[ii] =  file_df['iL'].values if file_df['iL'].values[-1] > 0 else - file_df['iL'].values
    u[ii] =  file_df['u'].values
    uD[ii] =  file_df['uD'].values
    iD[ii] =  file_df['iD'].values if file_df['iD'].values[-1] > 0 else - file_df['iD'].values
    iLc[ii] =  file_df['iLc'].values if file_df['iLc'].values[-1] > 0 else - file_df['iLc'].values
    uc[ii] =  file_df['uc'].values
    uDc[ii] =  file_df['uDc'].values
    iDc[ii] =  file_df['iDc'].values if file_df['iDc'].values[-1] > 0 else - file_df['iDc'].values
    #  '''

    # Store all data in one pandas dataframe
    if file_df['iL'].iloc[-1] < 0:
        file_df['iL'] = - file_df['iL']
    if file_df['iD'].iloc[-1] < 0:
        file_df['iD'] = - file_df['iD']
    if file_df['iLc'].iloc[-1] < 0:
        file_df['iLc'] = - file_df['iLc']
    if file_df['iDc'].iloc[-1] < 0:
        file_df['iDc'] = - file_df['iDc']

    #  Get new parameters from filename and add them to new columns in the dataframe
    string = filelist[ii].replace(".dat","");
    sample = re.compile('/\D+\d.\d-\D\d\d\d').findall(string)[0];
    sample = re.sub('/','', sample)
    file_df['sample'] = sample
    if re.search('-\D125', sample):
        maskGen = 2
    else:
        maskGen = 3
    file_df['maskGen'] = maskGen
    diam = re.compile('\d+\d').findall(string)[-1]
    file_df['diam'] = int(diam)
    dev = re.compile('\D+\d').findall(string)[-1]
    file_df['dev'] = dev
    file_df['T'] = 300
    #  '''
    #dTM (distance between Top contact and top Mesa)
    #dMM (distance between Middle contact and top Mesa)
    if maskGen == 3:
        if re.search('[a-bA-B][0-9]', dev):
            file_df['dTM'] = 50
            if re.search('[0-4]', dev):
                file_df['dMM'] = 25
            else:
                file_df['dMM'] = 50
        elif re.search('[c-dC-D][0-9]', dev):
            file_df['dTM'] = 75
            if re.search('[0-4]', dev):
                file_df['dMM'] = 25
            else:
                file_df['dMM'] = 75
        elif re.search('[e-fE-F][0-9]', dev):
            file_df['dTM'] = 95
            if re.search('[0-4]', dev):
                file_df['dMM'] = 5
            else:
                file_df['dMM'] = 95
    elif maskGen == 2:
        if re.search('[C-H]', dev):
            if re.search('[0-4]', dev):
                file_df['dTM'] = file_df['dMM'] = 5
            elif re.search('[5-9]', dev):
                file_df['dTM'] = file_df['dMM'] = 10
        if re.search('[J-L]', dev):
            if re.search('[0-4]', dev):
                file_df['dTM'] = file_df['dMM'] = 20
            elif re.search('[5-9]', dev):
                file_df['dTM'] = file_df['dMM'] = 40
    #'''

    # Append extracted dataframe to the general df
    df = df.append(file_df)

# Go back to initial directory
os.chdir(initial_path)

print("*** Elapsed time: %.2f s (Loaded %.f parameters from %.f files)" %(time.time()-st, df.shape[1], len(filelist)))


# #################################
# Calculate element-wise parameters
# #################################
print("*** Elapsed time: %.2f s (Calculate element-wise parameters)" %(time.time()-st))

#  df['dC'] = df['dTM'] + df['dMM']

rExt = 3.75 # external resistance
df['r'] = np.gradient(df.set_index('u')['iL'])
df['uL'] = df['u'] - df['iL']*rExt # internal bias

df['cqe'] = df['iD'] / df['iL']

Eg = 1.42 # TODO add T dependence
df['pce'] = df['cqe'] * Eg / df['u']
df['pceL'] = df['cqe'] * Eg / df['uL']

#  print('df:\n',df)

print("*** Elapsed time: %.2f s (Done)" %(time.time()-st))


# ###########################
# Calculate device parameters
# ###########################
print("*** Elapsed time: %.2f s (Calculate device parameters)" %(time.time()-st))

# Maximum values
# --------------
max_vals = df[df['u'] > 0.9].groupby(['sample', 'diam', 'dev', 'maskGen', 'T','dTM','dMM'], as_index=True)['cqe','pce','iL','r'].max()
max_vals.rename(columns = {'cqe':'cqem','pce':'pcem', 'iL':'iLm', 'r':'rm'}, inplace = True)
#  print('max_vals:\n',max_vals)
cqem = df[df['u'] > 0.9].groupby(['dTM','dMM','sample', 'diam', 'dev', 'maskGen', 'T'], as_index=True)['cqe'].max()
cqem.rename(columns = {'cqe':'cqem'}, inplace = True).unstack()
#  cqem = iD / iL
print(cqem.shape)

print("*** Elapsed time: %.2f s (Done)" %(time.time()-st))


#  #####
#  Plots
#  #####
#  print(plt.style.available)
#  plt.style.use('ggplot')
plt.style.use('dark_background')

print("*** Elapsed time: %.2f s (Plot figures)" %(time.time()-st))

fig, axes = plt.subplots()
cqem.plot(legend=True, style='o')

# TODO plot individual curves for each device but grouped per e.g. size and dMM (clustering?)

#  '''
## Groupby
## -------
# IV
fig, axes = plt.subplots()
df[df['diam'] == 1000].set_index('u').groupby(['dTM', 'dMM'])['iL'].plot(legend=True, style='o')
# CQE, PCE
fig, axes = plt.subplots(nrows=1, ncols=2, sharex=False, sharey=False)
df[df['dev'] == 'C8'].set_index('u').groupby(['dTM','dMM'])['cqe'].plot(legend=True, ax=axes[0], xlim=(1,3), ylim=(0,1))
df.set_index('u').groupby(['diam'])['pce'].plot(legend=True, ax=axes[1], xlim=(1,3), ylim=(0,1))
#  '''

#'''
fig, ax = plt.subplots()

for key, grp in df.groupby(['dMM','dTM','dev']):
    ax = grp.plot(ax=ax, kind='line', x='u', y='iL', label=key)
#'''

## Pivot
## -----
#  pivoted = df.pivot_table(index='u', columns=['diam','dMM','dev'])
pivoted = df.pivot_table(index='u', columns=['dTM'])
#  print('Pivoted df:\n',pivoted.head())
pivoted.stack(level=[0,1])
#  print('Pivoted df unstacked:\n',pivoted.head())
#  '''

#  '''
fig, axes = plt.subplots(nrows=2, ncols=2, sharex=False)
# IV
pivoted[pivoted['diam']== 250]['iL'].plot(ax=axes[0,0], xlim=(-1,4), style='o')
pivoted[pivoted['diam']== 500]['iL'].plot(ax=axes[0,1], xlim=(-1,4), style='o')
pivoted[pivoted['diam']==1000]['iL'].plot(ax=axes[1,0], xlim=(-1,4), style='o')
pivoted[pivoted['diam']==2000]['iL'].plot(ax=axes[1,1], xlim=(-1,4), style='o')
# CQE
fig, axes = plt.subplots(nrows=2, ncols=2, sharex=False, figsize=(14,4))
pivoted[pivoted['diam']== 250]['cqe'].plot(ax=axes[0,0], xlim=(1,3), ylim=(0,1), style='o')
pivoted[pivoted['diam']== 500]['cqe'].plot(ax=axes[0,1], xlim=(1,3), ylim=(0,1), style='o')
pivoted[pivoted['diam']==1000]['cqe'].plot(ax=axes[1,0], xlim=(1,3), ylim=(0,1), style='o')
pivoted[pivoted['diam']==2000]['cqe'].plot(ax=axes[1,1], xlim=(1,3), ylim=(0,1), style='o')
#  '''



print("*** Elapsed time: %.2f s (Done)" %(time.time()-st))

plt.show()
