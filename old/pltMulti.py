import itertools
import numpy as np
import matplotlib.pyplot as plt

def pltMulti(plot_style, x, y, discard, N, mapcolors, l_styles, m_styles, xlabel, ylabel, xlim, ylim):
    plt.figure()
    for ii,(color, linestyle, marker) in zip(range(N),itertools.product(mapcolors, l_styles, m_styles)):
        if not discard[ii]:
            plt.plot_style(x[ii], y[ii],
                    #  color = colorDist[ii], # color each curve according to their contact distances
                    color = color,
                    linestyle = linestyle,
                    marker = marker,
                    #  label = r'%s-%s-%s' %(sampleName[ii],samplePID[ii],devID[ii])
                    )
    plt.xlim(xlim)
    plt.ylim(ylim)
    plt.legend(ncol=4,
            loc='best',
            #  loc='center left', bbox_to_anchor=(0.5, 0.4),
            columnspacing=1.0, labelspacing=0.0,
            handletextpad=0.0, handlelength=1.5,
            fancybox=True, shadow=True,
            )
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
