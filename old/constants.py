import numpy as np

def elementary():
    q = 1.6e-19 #C
    k = 1.38e-23 #J/K
    return q, k

def Tdep(T):
    # Constants
    q, k = elementary()
    T = T.astype(float) if hasattr(T, "__len__") else T
    kT = k * T

    Vt = kT/q
    Vr =  2.4e-13 #active region volume


    #parameters from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    Eg = 1.519 - 5.405e-4 * T**2 / (T + 204)
    El = 1.815 - 6.05e-4 * T**2 / (T + 204)
    Ex = 1.981 - 4.6e-4 * T**2 / (T + 204)
    Nv = 1.83e21 * T**1.5
    Nc = 8.63e19*T**1.5*(1 - 1.931e-4*T - 4.19e-8*T**2 + 21 * np.exp(q * (Eg - El) / 2 / kT) + 44 * np.exp(q *(Eg - Ex) / 2 / kT))

    #intrinsic carrier density
    #  ni = 2.1e12
    ni = np.sqrt(Nc * Nv * np.exp(-q * Eg / kT))

    hwave = Eg + 1.8 * kT / q

    return kT, Vt, Vr, Eg, El, Ex, Nv, Nc, ni, hwave
