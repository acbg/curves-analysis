import os
import numpy as np
import pandas as pd
import re
import pathlib
import itertools
from tabulate import tabulate
import matplotlib.pyplot as plt

q, k = np.genfromtxt('constants.csv')

def Vt(T):
    return k * T / q

def Eg (mat, T):
    '''
    parameters from IOFFE, http://www.ioffe.ru/SVA/NSM/Semicond/GaAs/bandstr.html#Masses
    '''
    if mat == 'GaAs':
        Eg = 1.519 - 5.405e-4 * T**2 / (T + 204)
    return Eg

def El (mat, T):
    if mat == 'GaAs':
        El = 1.815 - 6.05e-4 * T**2 / (T + 204)
    return El

def Ex (mat, T):
    if mat == 'GaAs':
        Ex = 1.981 - 4.6e-4 * T**2 / (T + 204)
    return Ex

def Nv (mat, T):
    if mat == 'GaAs':
        Nv = 1.83e21 * T**1.5
    return Nv

def Nc (mat, T):
    if mat == 'GaAs':
        Nc = 8.63e19*T**1.5*(1 - 1.931e-4*T - 4.19e-8*T**2 + 21 * np.exp(q * (Eg(mat,T) - El(mat,T)) / 2 / k / T) + 44 * np.exp(q *(Eg(mat,T) - Ex(mat,T)) / 2 / k / T))
    return Nc

def intrinsicCarrierDensity(mat, T):
    '''
    intrinsic carrier density
    '''
    #  ni = 2.1e12
    if mat == 'GaAs':
        ni = np.sqrt (Nc (mat,T) * Nv (mat,T) * np.exp (-q * Eg (mat,T) / k / T))
    return ni

def hwave(mat, T):
    if mat == 'GaAs':
        hwave = Eg (mat, T) + 1.8 * k * T / q
    return hwave

#####################
# Files manipulation
#####################
def getRootDir(directory):
    '''
    Set the root directory.
    If no argument is given, it gives the default /m/nbe/..., and if that doesn't exist,
    it gives the same route mounted in the home directory.
    '''
    if directory:
        return directory
    else:
        defaultDir = '/m/nbe/project/quantum-data/ZIM/notes/iTPX/Samples/'
        homeDefaultDir = str(pathlib.Path.home()) + '/qdata/ZIM/notes/iTPX/Samples/'
        if pathlib.Path(defaultDir).exists():
            return defaultDir
        elif pathlib.Path(homeDefaultDir).exists():
            return homeDefaultDir
        else:
            print('####The root directory does not exist!!!!!')
            return

def getDirList(case):
    '''
    Gives a directory list corresponding to the case being performed
    '''
    if case == 'ContactDistance':
        dirList = [
                'G/Aalto3.1/S125/IVdata/',
                'G/Aalto3.1/S126/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0341/S129/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0341/S134/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0341/S135/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0342/S130/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0342/S132/IVdata/',
                ]
    if case == 'Passivation':
        dirList = [
                'TPX/TPX0300-TPX0399/TPX0341/S134/passivation/',
                'TPX/TPX0300-TPX0399/TPX0341/S135/passivation/',
                ]
    if case == 'Tdep':
        dirList = [
                'TPX/TPX0300-TPX0399/TPX0341/S134/tdep/',
                ]

    if case == 'PV':
        dirList = [
                'K/K4733/K4733-S83/data/',
                'TPX/TPX0300-TPX0399/TPX0341/S129/IVdata/',
                'TPX/TPX0300-TPX0399/TPX0342/S132/IVdata/',
                ]

    return dirList

def getFiles(rootDir, dirs, extensions, showFiles=False):
    '''
    Get file names with different extensions from a set of directories contained in a root directory
    '''
    all_files = []
    origDir = os.getcwd()
    os.chdir(rootDir)
    for d in dirs:
        for ext in extensions:
            all_files.extend(pathlib.Path(rootDir,d).glob(ext))
    os.chdir(origDir)
    if showFiles:
        for item in all_files: print(item)
    return all_files

###############################
# Get and calculate parameters (stored in arrays)
###############################
def booleanIndexing(v, fillval=np.nan):
    '''
    Convert missing valuees to a certain value (to np.nan by default)
    '''
    lens = np.array([len(item) for item in v])
    mask = lens[:,None] > np.arange(lens.max())
    out = np.full(mask.shape,fillval)
    out[mask] = np.concatenate(v)


    return out

def createDF (
        sampleName, samplePID, diameter, T, devID, device, meas, comments,
        isPassivated, discard, area, iRev, iFor, rs, condMax, cqem, cqec,
        cqemI, cqemV, pcem, srh, ideality, has4pp, rs4pp, cqem4pp, cqec4pp,
        cqemI4pp, cqemV4pp, pce4ppm, srh4pp, ideality4pp,
        saveTable=False, fileName=None, printTable=False
        ):
    '''
    Create a dataFrame with all devices properties
    '''
    df = pd.DataFrame({
        'Sample': sampleName,
        'SamplePID': samplePID,
        'Diameter': diameter,
        'T': T,
        'DevID': devID,
        'Device': device,
        'Meas': meas,
        'Comment': comments,
        'isPassivated': isPassivated,
        'Discard': discard,
        'Area': area,
        'Irev': iRev,
        'Ifor': iFor,
        'Rs': rs,
        'CondMax': condMax,
        #  'U(CondMax)': uCondMax,
        'CQEm': cqem,
        'CQEc': cqec,
        'CQEmI': cqemI,
        'CQEmV': cqemV,
        'PCEm': pcem,
        'A': srh,
        'Ideality': ideality,
        #  4pp
        'has4pp': has4pp,
        'Rs4pp': rs4pp,
        'CQEm4pp': cqem4pp,
        'CQEc4pp': cqec4pp,
        'CQEmI4pp': cqemI4pp,
        'CQEmV4pp': cqemV4pp,
        'PCEm4pp': pce4ppm,
        'A4pp': srh4pp,
        'Ideality4pp': ideality4pp,
        })

    # Mask generation
    # ---------------
    df['MaskGen'] = 3
    df['MaskGen'] = np.where (df.SamplePID == 'S125', 2, df.MaskGen)
    df['MaskGen'] = np.where (df.SamplePID == 'S126', 3, df.MaskGen)
    df['MaskGen'] = np.where (df.SamplePID == 'S129', 3, df.MaskGen)
    df['MaskGen'] = np.where (df.SamplePID == 'S130', 3, df.MaskGen)
    df['MaskGen'] = np.where (df.SamplePID == 'S132', 2, df.MaskGen)
    df['MaskGen'] = np.where (df.SamplePID == 'S134', 3, df.MaskGen)
    df['MaskGen'] = np.where (df.SamplePID == 'S135', 2, df.MaskGen)
    MaskGen = df.MaskGen.values

    # Contact distances
    # -----------------
    # Distance from Top Mesa to Middle Contact
    # ----------------------------------------
    df['dT'] = 0
    # ----------------------------------------------------------------------------------------------
    df['dT'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[a-hA-H][0-4]')),  5, df.dT)
    df['dT'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[a-hA-H][5-9]')), 10, df.dT)
    df['dT'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[i-lI-L][0-4]')), 20, df.dT)
    df['dT'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[i-lI-L][5-9]')), 40, df.dT)
    # ----------------------------------------------------------------------------------------------
    df['dT'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[a-bA-B][0-9]')), 50, df.dT)
    df['dT'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[c-dC-D][0-9]')), 75, df.dT)
    df['dT'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[e-fE-F][0-9]')), 95, df.dT)
    # Create an array
    dT = df.dT.values
    # ----------------------------------------------------------------------------------------------

    # Distance from Middle Contact to Middle Mesa
    # ----------------------------------------
    df['dM'] = 0
    # ----------------------------------------------------------------------------------------------
    df['dM'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[a-hA-H][0-4]')),  5, df.dM)
    df['dM'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[a-hA-H][5-9]')), 10, df.dM)
    df['dM'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[i-lI-L][0-4]')), 20, df.dM)
    df['dM'] = np.where ((df.MaskGen == 2) & (df.Device.str.contains('[i-lI-L][5-9]')), 40, df.dM)
    # ----------------------------------------------------------------------------------------------
    df['dM'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[a-bA-B][0-4]')), 25, df.dM)
    df['dM'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[a-bA-B][5-9]')), 50, df.dM)
    df['dM'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[c-dC-D][0-4]')), 25, df.dM)
    df['dM'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[c-dC-D][5-9]')), 75, df.dM)
    df['dM'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[e-fE-F][0-4]')),  5, df.dM)
    df['dM'] = np.where ((df.MaskGen == 3) & (df.Device.str.contains('[e-fE-F][5-9]')), 95, df.dM)
    # ----------------------------------------------------------------------------------------------
    # Create an array
    dM = df.dM.values
    # ----------------------------------------------------------------------------------------------
    df['dC'] = df.dT + df.dM
    dC = df.dC.values

    # Select and reorder columns
    summary = df[[
        'Discard',
        'Sample',
        'SamplePID',
        'DevID',
        'Device',
        'Area',
        'Diameter',
        'T',
        'MaskGen',
        'isPassivated',
        'dM',
        'dT',
        'dC',
        'Irev',
        'Rs',
        'CondMax',
        #  'U(CondMax)',
        'CQEm',
        'CQEmV',
        'PCEm',
        'CQEm4pp',
        'PCEm4pp',
        'A',
        'Ideality',
        'Ideality4pp',
        #  'Comment',
        ]]

    # Print the summary
    # -----------------
    # Set columns format
    #  pd.options.display.float_format = '{:,.3f}'.format
    summary['Area'] = summary['Area'].map('{:,.2e}'.format)
    summary['Rs'] = summary['Rs'].map('{:,.2f}'.format)
    #  summary['Diameter'] = summary['Diameter'].map('{:,.5e}'.format)
    summary['Irev'] = summary['Irev'].map('{:,.2e}'.format)
    summary['CondMax'] = summary['CondMax'].map('{:,.2f}'.format)
    #  summary['U(CondMax)'] = summary['U(CondMax)'].map('{:,.2f}'.format)
    summary['CQEm'] = summary['CQEm'].map('{:,.3f}'.format)
    summary['PCEm'] = summary['PCEm'].map('{:,.3f}'.format)
    summary['CQEm4pp'] = summary['CQEm4pp'].map('{:,.2f}'.format)
    summary['PCEm4pp'] = summary['PCEm4pp'].map('{:,.3f}'.format)
    summary['A'] = summary['A'].map('{:,.2e}'.format)
    #  print(summary.sort('rm'))

    #  table= tabulate(summary.sort_values(by='Meas'), headers='keys', tablefmt='simple')
    table = tabulate(summary.sort_values(['CQEm','isPassivated', 'Diameter', 'Device']), headers='keys', tablefmt='simple')
    #  table= tabulate(summary, headers='keys', tablefmt='github')

    # Print to stdout
    if printTable:
        print(table)

    # ... and to a file
    if saveTable:
        f = open(fileName, 'w')
        f.write(tabulate(summary.sort_values(by='dM'), headers='keys', tablefmt='github'))
        f.close()

    return df, summary, table, MaskGen, dT, dM, dC

def getParams(filelist):
    '''
    Get parameters from a bunch of files
    '''
    # Define objects
    # --------------
    u, uD, iL, iD, uc, uDc, iLc, iDc, \
            has4pp, u4pp, uD4pp, iL4pp, \
            iD4pp, u4ppc, uD4ppc, iL4ppc, \
            iD4ppc, T, samplePID, sampleName, \
            diameter, device, devID, meas, comments, \
            ARthickness, isPassivated, isDark = \
            [np.array(np.zeros(len(filelist)),dtype=object) for _ in range(28)]

    # Read the files
    # --------------
    for ii in range(len(filelist)):

        filename = filelist[ii]

        #  Get parameters from filename and store them in ndarray
        with filename.open() as f:
            try:
                comment = f.readlines()[19]
                comment = comment.split('# ', 1)[-1]
                comment = comment.split('\n', 1)[0]
                comments[ii] = comment
            finally:
                f.close()

        sampID = filename.parent.as_posix().split('IVdata')[0]
        samplePID[ii] = sampID.split('/')[-2]
        sampleName[ii] = sampID.split('/')[-3]

        devID[ii] = filename.stem # Device complete name

        device[ii] = re.findall('[a-zA-Z0-9]+[A-Z0-9]', devID[ii])[0]

        isPassivated[ii] = True if devID[ii][-1] == 'P' else False
        isDark[ii] = True if devID[ii][-4:] == 'Dark' else False

        diam = re.split('[A-Z]+', devID[ii])[0]
        diameter[ii] = 1e-6 * float(diam) # in meters

        meas[ii] = re.findall('[a-zA-Z]', devID[ii])[-1]

        if 'T' in devID[ii]:
            temperature = devID[ii].split('T')[1]
            T[ii] = float(re.sub('\D','',temperature))
        else:
            T[ii] = 300

        ARthickness[ii] = 300e-9

        # Does the file contain 4pp measurements?
        has4pp[ii] = True if comments[ii] == '#Both 2 and 4 wire measurements' or comments[ii] == '#4 wire measurements' else False
        #  has4pp[ii] = True if ' VS_pulse_4pr' or ' VS' in dfi else False

        #  Read csv file and add the IV data to a dataframe
        dfi = pd.read_csv(filename, delimiter=',', comment='#')

        # Store data in separated numpy ndarrays
        if ' VS' in dfi:
            u[ii]  =  dfi[' VS'].values
            uD[ii] =  dfi[' VD'].values
            iL[ii] =  dfi['IS'].values if dfi['IS'].values[-1] > 0 else -dfi['IS'].values
            iD[ii] =  dfi[' ID'].values if dfi[' ID'].values[-1] > 0 else -dfi[' ID'].values
            # -----------------------------
            uc[ii]  =  np.zeros(len(filelist))
            uDc[ii] =  np.zeros(len(filelist))
            iLc[ii] =  np.zeros(len(filelist))
            iDc[ii] =  np.zeros(len(filelist))
            # -----------------------------
            u4pp[ii]  =  dfi[' VS'].values
            uD4pp[ii] =  dfi[' VD'].values
            iL4pp[ii] =  dfi['IS'].values if dfi['IS'].values[-1] > 0 else -dfi['IS'].values
            iD4pp[ii] =  dfi[' ID'].values if dfi[' ID'].values[-1] > 0 else -dfi[' ID'].values
        elif ' VS_pulse' in dfi:
            u[ii]  =  dfi[' VS_pulse'].values
            uD[ii] =  dfi[' VD_pulse'].values
            iL[ii] =  dfi['IS_pulse'].values if dfi['IS_pulse'].values[-1] > 0 else -dfi['IS_pulse'].values
            iD[ii] =  dfi[' ID_pulse'].values if dfi[' ID_pulse'].values[-1] > 0 else -dfi[' ID_pulse'].values
            # -----------------------------
            uc[ii]  =  dfi[' VS_cont'].values
            uDc[ii] =  dfi[' VD_cont'].values
            iLc[ii] =  dfi[' IS_cont'].values if dfi[' IS_cont'].values[-1] > 0 else -dfi[' IS_cont'].values
            iDc[ii] =  dfi[' ID_cont'].values if dfi[' ID_cont'].values[-1] > 0 else -dfi[' ID_cont'].values
            # -----------------------------
            u4pp[ii]  =  np.zeros(len(filelist))
            uD4pp[ii] =  np.zeros(len(filelist))
            iL4pp[ii] =  np.zeros(len(filelist))
            iD4pp[ii] =  np.zeros(len(filelist))
        elif ' VS_pulse_4pr' in dfi:
            u[ii]  =  dfi[' VS_pulse_2pr'].values
            uD[ii] =  dfi[' VD_pulse_2pr'].values
            iL[ii] =  dfi[' IS_pulse_2pr'].values if dfi[' IS_pulse_2pr'].values[-1] > 0 else -dfi[' IS_pulse_2pr'].values
            iD[ii] =  dfi[' ID_pulse_2pr'].values if dfi[' ID_pulse_2pr'].values[-1] > 0 else -dfi[' ID_pulse_2pr'].values
            # -----------------------------
            uc[ii]  =  dfi[' VS_cont_2pr'].values
            uDc[ii] =  dfi[' VD_cont_2pr'].values
            iLc[ii] =  dfi[' IS_cont_2pr'].values if dfi[' IS_cont_2pr'].values[-1] > 0 else -dfi[' IS_cont_2pr'].values
            iDc[ii] =  dfi[' ID_cont_2pr'].values if dfi[' ID_cont_2pr'].values[-1] > 0 else -dfi[' ID_cont_2pr'].values
            # -----------------------------
            u4pp[ii]  =  dfi[' VS_pulse_4pr'].values
            uD4pp[ii] =  dfi[' VD_pulse_4pr'].values
            iL4pp[ii] =  dfi['IS_pulse_4pr'].values if dfi['IS_pulse_4pr'].values[-1] > 0 else -dfi['IS_pulse_4pr'].values
            iD4pp[ii] =  dfi[' ID_pulse_4pr'].values if dfi[' ID_pulse_4pr'].values[-1] > 0 else -dfi[' ID_pulse_4pr'].values
            # -----------------------------
            u4ppc[ii]  =  dfi[' VS_cont_4pr'].values
            uD4ppc[ii] =  dfi[' VD_cont_4pr'].values
            iL4ppc[ii] =  dfi[' IS_cont_4pr'].values if  dfi[' IS_cont_4pr'].values[-1] > 0 else  -dfi[' IS_cont_4pr'].values
            iD4ppc[ii] =  dfi[' ID_cont_4pr'].values if dfi[' ID_cont_4pr'].values[-1] > 0 else -dfi[' ID_cont_4pr'].values

    ############################################################################

    # Set arrays type
    # ---------------
    #  has4pp = has4pp.astype(bool)

    # Fill arrays with NaNs to have homogeneous size arrays
    # -----------------------------------------------------
    u  = booleanIndexing(u,  fillval=np.nan)
    uD = booleanIndexing(uD, fillval=np.nan)
    iL = booleanIndexing(iL, fillval=np.nan)
    iD = booleanIndexing(iD, fillval=np.nan)
    uc  = booleanIndexing(uc,  fillval=np.nan)
    uDc = booleanIndexing(uDc, fillval=np.nan)
    iLc = booleanIndexing(iLc, fillval=np.nan)
    iDc = booleanIndexing(iDc, fillval=np.nan)
    u4pp  = booleanIndexing(u4pp,  fillval=np.nan)
    uD4pp = booleanIndexing(uD4pp, fillval=np.nan)
    iL4pp = booleanIndexing(iL4pp, fillval=np.nan)
    iD4pp = booleanIndexing(iD4pp, fillval=np.nan)
    #  u4ppc  = booleanIndexing(u4ppc,  fillval=np.nan)
    #  uD4ppc = booleanIndexing(uD4ppc, fillval=np.nan)
    #  iL4ppc = booleanIndexing(iL4ppc, fillval=np.nan)
    #  iD4ppc = booleanIndexing(iD4ppc, fillval=np.nan)


    return u, uD, iL, iD, uc, uDc, iLc, iDc, has4pp, u4pp, uD4pp, iL4pp, iD4pp, u4ppc, uD4ppc, iL4ppc, iD4ppc, T, samplePID, sampleName, diameter, device, devID, meas, comments, ARthickness, isPassivated, isDark

def calculateParams(mat, u, uD, iL, iD, uc, uDc, iLc, iDc, has4pp, u4pp, uD4pp, iL4pp, iD4pp, u4ppc, uD4ppc, iL4ppc, iD4ppc, T, samplePID, sampleName, diameter, device, devID, meas, comments, ARthickness, isPassivated, isDark):
    '''
    Calculate element-wise parameters
    '''

    # TODO chi
    # TODO Rsurface
    # TODO Isurf

    # Area and current density
    # ------------------------
    area = np.pi * diameter**2
    jL = area[:, None] * iL * 1e4 # A/cm2
    jL = jL.astype(float)
    jL4pp = area[:, None] * iL4pp * 1e4 # A/cm2
    jL4pp = jL4pp.astype(float)

    # Resistance
    # ----------
    rExt = 3.75 # external resistance
    cond = np.diff(iL) / np.diff(u) # conductance, gradient does not work...
    condMax = np.nanmax(cond, axis=1)
    #  condMaxIdx = np.nanargmax(cond, axis=1)
    #  uCondMax = [u[i][condMaxIdx[i]] for i in range(len(u))]
    resistance = 1 / cond
    rs = 1 / condMax

    cond4pp = np.diff(iL4pp) / np.diff(u4pp) # conductance, gradient does not work...
    condMax4pp = np.nanmax(cond4pp, axis=1)
    #  condMaxIdx = np.nanargmax(cond, axis=1)
    #  uCondMax = [u[i][condMaxIdx[i]] for i in range(len(u))]
    resistance4pp = 1 / cond4pp
    rs4pp = 1 / condMax4pp

    # Ideality factor
    # ---------------
    dx = np.diff (np.log (jL)) / np.diff (u)
    idealityF = 1 / dx / Vt(T)
    ideality = np.nanmax ((u[:,:-1] > 0.9) * (u[:,:-1] < 1.1) * (idealityF < 3.0) * idealityF, axis=1)
    dx4pp = np.diff (np.log (iL4pp)) / np.diff (u4pp)
    idealityF4pp = 1 / dx4pp / Vt(T)
    ideality4pp = np.nanmax ((u4pp[:,:-1] > 0.9) * (u4pp[:,:-1] < 1.1) * (idealityF4pp < 3.0) * idealityF4pp, axis=1)
    ideality4ppIdx = np.argmax ((u4pp[:,:-1] > 0.9) * (u4pp[:,:-1] < 1.1) * (idealityF4pp < 3.0) * idealityF4pp, axis=1)

    uL = u - iL * rs[:,None] # internal bias
    uL4pp = u4pp - iL4pp * rs4pp[:,None] # internal bias

    # ABC parameters
    # --------------
    kkk=q * intrinsicCarrierDensity(mat, T) * ARthickness
    srh = (u>0.5) * (iL > (1.5 * np.nanmin (np.abs (iL)))) * iL / np.exp (np.abs (u)) / 2 / Vt(T) / kkk[:, None]
    srh = np.nanmax (srh.astype(float), axis=1)
    srh4pp = (u4pp>0.5) * (iL4pp > (1.5 * np.nanmin (np.abs (iL4pp)))) * iL4pp / np.exp (np.abs (u4pp)) / 2 / Vt(T) / kkk[:,None]
    srh4pp = np.nanmax (srh4pp.astype(float), axis=1)

    # CQE
    # ---
    cqe = iD / iL
    cqem = np.nanmax(cqe*(u>1.1), axis = 1)
    cqemIdx = np.argmax(cqe*(u>1.1), axis = 1)
    cqemI = [iL[i][cqemIdx[i]] for i in range(len(iL))]
    cqemV = [u[i][cqemIdx[i]] for i in range(len(u))]
    cqec = np.nanmax(cqe*(u<1.2)*(u>1.1), axis = 1)

    cqe4pp = iD4pp / iL4pp
    cqem4pp = np.nanmax(cqe4pp*(u4pp>0.9), axis = 1)
    cqemIdx4pp = np.argmax(cqe4pp*(u4pp>0.9), axis = 1)
    cqemI4pp = [iL4pp[i][cqemIdx4pp[i]-1] for i in range(len(iL4pp))]
    cqemV4pp = [u4pp[i][cqemIdx4pp[i]-1] for i in range(len(u4pp))]
    cqec4pp = np.nanmax(cqe4pp*(u4pp<1.21)*(u4pp>1.0), axis = 1)


    # PCE
    # ---
    pce = cqe * Eg(mat, T) / u
    pcem = np.nanmax((u>1.1)*pce, axis = 1)
    pce4pp = cqe4pp * Eg(mat, T) / u4pp
    pcem4pp = np.nanmax((u4pp>1.1)*pce4pp, axis = 1)

    # I_reverse and I_forward
    # -----------------------
    iRev = iL[:,0]
    iFor = iL[:,-1]

    # A 'discard' array, based on:
    # -----------------------------------
    # very high reverse (leakage) current,
    discard = np.where (abs(iRev) > 1e-7, True, False)
    # very low forward current,
    discard = np.where (abs(iFor) < 1e-2, True, discard)
    # TODO: if isDDS, else no discard for non-DDS devices
    #  # extremely low cqem
    discard = np.where (abs(cqem) < 0.35, True, discard)
    discard = np.where (abs(cqem4pp) < 0.35, True, discard)
    # extremely high cqem
    discard = np.where (abs(cqem) > 0.90, True, discard)
    discard = np.where (abs(cqem4pp) > 0.90, True, discard)

    # TODO An array to store the 'mask generation'
    # ---------------------------------------
    #  mask = np.array(list(map(lambda x: bool(re.match(r'S125', x)), samplePID)))
    #  maskGen = np.where (mask, 2, 3)
    #  mask = np.array(list(map(lambda x: bool(re.match(r'S12[6,9]', x)), samplePID)))
    #  maskGen = np.where (mask, 3, maskGen)
    # An array to store the 'contact distances'
    # ---------------------------------------
    #  mask = np.array(list(map(lambda x: bool(re.match(r'[a-hA-H][0-4]', x)), device)))
    #  dT = np.where ((maskGen == 2) & mask, 5, 0)

    return area, jL, jL4pp, cond, condMax, resistance, rs, cond4pp, condMax4pp, resistance4pp, rs4pp, ideality, ideality4pp, uL, uL4pp, srh, srh4pp, cqe, cqem, cqemI, cqemV, cqec, cqe4pp, cqem4pp, cqemI4pp, cqemV4pp, cqec4pp, pce, pcem, pce4pp, pcem4pp, iRev, iFor, discard


def selDevs2vars (df, outVar, maxVar, minVar, *args, showDF=False):
    '''
    Select the best devices in a dataFrame which have the maximum 'maxVar' and minimum 'minVar'
    with their corresponding outVar, for each group of values of args
    '''

    df1 = df[df.groupby(args, sort=False)[maxVar].transform('max').eq(df[maxVar])]
    cols = [arg for arg in args] + [val for val in outVar] + [maxVar, minVar]
    #  mask = df1.groupby(args, sort=False)[minVar].idxmin()
    mask = df1.groupby(args, sort=False)[minVar].transform('min').eq(df1[minVar])
    dg = df1.loc[mask, cols]

    if showDF:
        print(dg)

    return dg

def df2dict (df, maingroup, subgroup, selectedCol):

    selDict = {k: f.groupby(subgroup)[selectedCol].apply(list).to_dict()
     for k, f in df.groupby(maingroup)}

    return selDict


def discardManual(case, discard, sampleName, samplePID, devID, device):
    '''
    Discard devices manually, from a visual inspection or any other means
    '''

    if case == 'ContactDistance':

        discard = np.where ((sampleName=='Aalto3.1') & (samplePID=='S125') & (devID== '500H4'), True, discard)
        discard = np.where ((sampleName=='Aalto3.1') & (samplePID=='S126') & (devID== '250D4'), True, discard)
        discard = np.where ((sampleName=='Aalto3.1') & (samplePID=='S126') & (devID== '250D5'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S129') & (devID== '250E7'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S129') & (devID== '250E8'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S129') & (devID== '250E9'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S129') & (devID== '250F8'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S129') & (devID== '250F9'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S134') & (devID=='1000C8'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S134') & (devID=='500E6P'), True, discard)
        discard = np.where ((sampleName=='TPX0341')  & (samplePID=='S135') & (devID=='500K2P'), True, discard)
        discard = np.where ((sampleName=='TPX0342')  & (samplePID=='S130') & (devID=='1000A5'), True, discard)
        discard = np.where ((sampleName=='TPX0342')  & (samplePID=='S130') & (devID=='1000E0'), True, discard)

        mask = np.array(list(map(lambda x: bool(re.match(r'250A[0-9]', x)), devID)))
        discard = np.where ((sampleName=='TPX0341') & (samplePID=='S129') & (mask), True, discard)
        discard = np.where ((sampleName=='TPX0342') & (samplePID=='S130') & (mask), True, discard)

    return discard

def selectedDevs(case, sampName=None, sampPID=None, devs=None):
    '''
    '''
    # TODO select only those devices from one sample and PID
    if case == 'All':
        selDevs = {}
        for (name, pid), group in itertools.groupby(zip(sampName, sampPID, devs), lambda k: (k[0], k[1])):
            for _, _, d in group:
                selDevs.setdefault(name, {}).setdefault(pid, []).append(d)

    elif case == 'CDist2pp':
        selDevs = {
                'TPX0341': {                # dT, dM
                    'S129': [
                        '250D1',            # 75, 25
                        '250D5',            # 75, 75
                        '250F4',            # 95,  5
                        '500C1',            # 75, 25
                        '500D5',            # 75, 75
                        '500E4',            # 95,  5
                        '1000D0',           # 75, 25
                        '1000D9',           # 75, 75
                        '1000E3',           # 95,  5
                        '1000E8',           # 95, 95
                        ],
                    'S134': [
                        '500C1',            # 75, 25
                        '500E4',            # 95,  5
                        '500E9',            # 95, 95
                        '1000D0',           # 75, 25
                        '1000E3',           # 95,  5
                        '1000E8',           # 95, 95
                        ],
                    'S135': [
                        '250H4',            #  5,  5
                        '250H7',            # 10, 10
                        '250K4',            # 20, 20
                        '250K5',            # 40, 40
                        '500G5',            # 10, 10
                        '500H4',            #  5,  5
                        '500K1',            # 20, 20
                        '500K9',            # 40, 40
                        '1000H3',           #  5,  5
                        '1000G8',           # 10, 10
                        '1000K1',           # 20, 20
                        '1000K6',           # 40, 40
                        ],
                    },
                'TPX0342': {                # dT, dM
                    'S132': [
                        '250G4',            #  5,  5
                        '250G5',            # 10, 10
                        '250L0',            # 20, 20
                        '250L9',            # 40, 40
                        '500G4',            #  5,  5
                        '500G8',            # 10, 10
                        '500L4',            # 20, 20
                        '500L9',            # 40, 40
                        '1000H4',           #  5,  5
                        '1000G8',           # 10, 10
                        '1000L3',           # 20, 20
                        '1000L6',           # 40, 40
                        ],
                    'S130': [
                        '250D4',            # 75, 25
                        '250C7',            # 75, 75
                        '250E2',            # 95,  5
                        '250F7',            # 95, 95
                        '500A9',            # 50, 50
                        '500A4',            # 50, 25
                        '500D4',            # 75, 25
                        '500D9',            # 75, 75
                        '500F4',            # 95,  5
                        '500F8',            # 95, 95
                        '1000A4',           # 50, 25
                        '1000A9',           # 50, 50
                        '1000D3',           # 75, 25
                        '1000D9',           # 75, 75
                        '1000F0',           # 95,  5
                        '1000E5',           # 95, 95
                        ],
                }
            }

    elif case == 'CDist4pp':
        selDevs = {
                'TPX0341': {                # dT, dM
                    'S129': [
                        '1000A3',           # 50, 25
                        '1000A8',           # 50, 50
                        '1000D3',           # 75, 25
                        '1000D8',           # 75, 75
                        '1000E3',           # 95,  5
                        '1000E8',           # 95, 95
                        '1000H3',           #  5,  5
                        '1000G8',           # 10, 10
                        '1000K3',           # 20, 20
                        '1000K6',           # 40, 40
                        ],
                    'S134': [
                        '1000D3',           # 75, 25
                        '1000E3',           # 95,  5
                        '1000E8',           # 95, 95
                        ],
                    'S135': [
                        '1000G8',           # 75, 25
                        '1000H3',           # 95,  5
                        '1000K3',           # 95, 95
                        '1000K6',           # 95, 95
                        ],
                    },
                'TPX0342': {                # dT, dM
                    'S132': [
                        '1000H4',           #  5,  5
                        '1000G8',           # 10, 10
                        '1000L3',           # 20, 20
                        '1000K8',           # 40, 40
                        ],
                    'S130': [
                        '1000A3',           # 50, 25
                        '',                 # 50, 50
                        '1000D3',           # 75, 25
                        '1000C7',           # 75, 75
                        '1000F3',           # 95,  5
                        '1000F8',           # 95, 95
                        ],
                    }
                }

    else:
        selDevs = {}


    return selDevs


##############
# Plots
##############

def plotEachDDS(device, devID, params, x, y, yMax, sty = 'o-'):

    for i, devi in enumerate(np.unique(device)):
            fig, ax = plt.subplots(2,2)
            fig.suptitle('Device %s, $d_{TM} = %.0f \mu$m, $d_{MM} = %.0f \mu$m' %(devi, params[0][device==devi][0], params[1][devID==devi][0]))
            for j, meas in enumerate(devID[device == devi]):

                axi=ax[0,0]
                axi.semilogy(x[devID == meas][0], abs(y[0][devID == meas][0]), sty, label = meas+' (LED)' )
                axi.semilogy(x[devID == meas][0], abs(y[1][devID == meas][0]), '--', label = meas+' (PD)')
                axi.set_xlabel('$U (V)$')
                axi.set_ylabel('$I (V)$')
                axi.legend(loc='best')
                axi=ax[1,0]
                axi.plot(x[devID == meas][0], abs(y[0][devID == meas][0]), sty, label=meas+' (LED)')
                axi.plot(x[devID == meas][0], abs(y[1][devID == meas][0]), '--', label=meas+' (PD)')
                axi.set_xlabel('$U (V)$')
                axi.set_ylabel('$I (V)$')
                axi.legend(loc='best')
                axi=ax[0,1]
                axi.plot(x[devID == meas][0], y[2][devID == meas][0], sty,
                        label=r'%s ($CQE_{max}=$%.2f)'%(meas, yMax[2][devID==meas][0]))
                axi.set_xlabel('$U (V)$')
                axi.set_ylabel('$CQE$')
                axi.set_xlim([0.8, 3])
                axi.set_ylim([0, 1])
                axi.legend(loc='best')
                axi=ax[1,1]
                axi.plot(x[devID == meas][0], y[3][devID == meas][0], sty,
                        label=r'%s ($PCE_{max}=$%.2f)'%(meas,yMax[3][devID==meas][0]))
                axi.set_xlabel('$U (V)$')
                axi.set_ylabel('$PCE$')
                axi.set_xlim([0.8, 3])
                axi.set_ylim([0, 1])
                axi.legend(loc='best')
    plt.show()

def plotIVAllDevs (u, uL, iL, iD, cqe, pce, colorStys, lineStys, markerStys, diameter, discard, device, sampleName, samplePID, devID, selDevs=None, savePlotDir=False, fileType='svg'):
    '''
    '''
    for i, diami in enumerate(np.unique(diameter)):

        fig = plt.figure()

        #  for ii in range(len(device)):
        for ii,(marker, linestyle, color) in zip(range(len(device)),itertools.product(markerStys, lineStys, colorStys)):

            if not discard[ii] and diameter[ii] == diami:# and sampleName[ii] in selDevs.keys():# (device[ii] in selDevs and samplePID[ii] in ):
                #TODO select from selDevs with a dictionary format

                plt.semilogy(u[ii], abs(iL[ii]),
                   color = color, linestyle = linestyle, marker = marker,
                   label = r'%s|%s|%s' %(sampleName[ii],samplePID[ii],devID[ii])
                   )

        plt.xlabel(r'$U$ (V)')
        plt.ylabel(r'$I$')
        plt.xlim([-0.5,3.0])
        nCurves = len (device [diameter == diami])
        nCols = 1 if nCurves < 34 else int(nCurves/35)
        plt.legend (ncol = nCols,
            loc = 'best',
            #  loc='left', bbox_to_anchor=(1.0, -0.1),
            columnspacing = 1.0, labelspacing = 0.0,
            handletextpad = 0.0, handlelength = 1.5,
            fancybox = True, shadow = True,
            )
        fig.suptitle ('Diameter: {:.0f} $\mu$m'.format (1e6*diami))
        if savePlotDir:
            plt.savefig(savePlotDir+r'IVallDevs-{:4.0f}'.format(1e6*diami)+'.'+fileType)

def plotAllDevs (u, uL, iL, iD, cqe, cqem, pce, rs, colorStys, lineStys, markerStys, dT, dM, diameter, discard, device, sampleName, samplePID, devID, chosenSample, chosenDT, chosenDM, showPD=False, selDevs=None, savePlotDir=False, fileType='svg'):
    '''
    '''
    if not selDevs:
        selDevs = fa.selectDevs('All', sampName=sampleName, sampPID=samplePID, devs=device)

    for i, diami in enumerate(np.unique(diameter)):

        fig, ax = plt.subplots(2,2)
        [ax0, ax1], [ax2, ax3] = ax

        #  for ii in range(len(device)):
        for ii,(marker, linestyle, color) in zip(range(len(device)),itertools.product(markerStys, lineStys, colorStys)):

            if not discard[ii] and \
                    diameter[ii] == diami and \
                    sampleName[ii] == chosenSample and \
                    dT[ii] == chosenDT and \
                    dM[ii] == chosenDM and \
                    sampleName[ii] in selDevs and \
                    samplePID[ii] in selDevs[sampleName[ii]] and \
                    device[ii] in selDevs[sampleName[ii]][samplePID[ii]]:

                ax0.semilogy(u[ii], abs(iL[ii]),
                   color = color, linestyle = linestyle, marker = marker,
                   label = r'%s|%s|%s-dT=%d,dM=%d,cqem=%.4f,rs=%.3f' %(sampleName[ii],samplePID[ii],devID[ii], dT[ii], dM[ii], cqem[ii], rs[ii])
                   )

                ax1.plot(u[ii], cqe[ii],
                #  ax[0,1].plot(smooth(uL[ii],25), smooth(cqe[ii],25), ':',
                   color = color, linestyle = linestyle, marker = marker,
                   label = r'%s|%s|%s' %(sampleName[ii],samplePID[ii],devID[ii])
                   )

                if showPD:
                    ax2.semilogy(u[ii], abs(iD[ii]),
                       color = color, linestyle = linestyle, marker = marker,
                       label = r'%s|%s|%s' %(sampleName[ii],samplePID[ii],devID[ii])
                       )
                else:
                    ax2.set_visible(False)

                ax3.plot(u[ii], pce[ii],
                   color = color, linestyle = linestyle, marker = marker,
                   label = r'%s|%s|%s' %(sampleName[ii],samplePID[ii],devID[ii])
                   )
        ax0.set_xlabel(r'$U$ (V)')
        ax0.set_ylabel(r'$I_L$')
        ax0.set_xlim([-0.5,3.0])
        ax1.set_xlabel(r'$U$ (V)')
        ax1.set_ylabel(r'$CQE$')
        ax1.set_xlim([0.8,2.5])
        ax1.set_ylim([0,1])
        #  ax2.set_xlabel(r'$U$ (V)')
        #  ax2.set_ylabel(r'$I_P$')
        #  ax2.set_xlim([-0.5,3.0])
        #  ax2.set_ylim([0,1])
        ax3.set_xlabel(r'$U$ (V)')
        ax3.set_ylabel(r'$PCE$')
        ax3.set_xlim([0.8,2.5])
        ax3.set_ylim([0,1])
        nCurves = len (device [(diameter == diami) & (sampleName == chosenSample)])
        nCols = 1 if nCurves < 30 else int(nCurves/30)
        ax0.legend (ncol = nCols,
            #  loc = 'best',
            loc='left', bbox_to_anchor=(1.1, -0.1),
            columnspacing = 1.0, labelspacing = 0.0,
            handletextpad = 0.0, handlelength = 1.5,
            fancybox = True, shadow = True,
            )
        fig.suptitle ('Diameter: {:.0f} $\mu$m'.format (1e6*diami))
        if savePlotDir:
            plt.savefig(savePlotDir+r'allDevs-{:4.0f}'.format(1e6*diami)+'.'+fileType)

def plotResistance(deviceList, samplePID, u, uL, iL, conductance, resistance, rs, discard, array4pp=None, plot4pp=False):
        # IV (and series resistance...)
        # #############################
        fig, ax = plt.subplots(2, 3)
        # ----------------------------------------------
        if plot4pp:
            fig.suptitle('4pp')
            deviceList = deviceList [array4pp==True]
            samplePID = samplePID [array4pp==True]
            u = u [array4pp==True]
            iL = iL [array4pp==True]
            conductance = conductance [array4pp==True]
            resistance = resistance [array4pp==True]
            rs= rs[array4pp==True]
            discard = discard [array4pp==True]

        for ii in range(len(deviceList)):
            if not discard[ii]:
                # IV
                axi = ax[0,0]
                axi.plot(u[ii], iL[ii], 'o-',
                  label = '[{sPID}|{dev}] $R_s = {rs:.2f} \Omega$ '.\
                          format(rs=rs[ii], sPID=samplePID[ii], dev=deviceList[ii])
                )
                axi.legend(loc='best', ncol=1)
                axi.set_xlabel(r'$U$ (V)')
                axi.set_ylabel(r'$I$ (A)')
                # IV semilog
                axi = ax[1,0]
                axi.semilogy(u[ii], iL[ii], 'o-')
                axi.set_xlabel(r'$U$ (V)')
                axi.set_ylabel(r'$I$ (A)')
                # IV (internal voltage)
                axi = ax[0,1]
                axi.plot(uL[ii], iL[ii], 'o-')
                axi.set_xlabel(r'$U_{LED}$ (V)')
                axi.set_ylabel(r'$I$ (A)')
                # IV (internal voltage)
                axi = ax[1,1]
                axi.semilogy(uL[ii], iL[ii], 'o-')
                axi.set_xlabel(r'$U_{LED}$ (V)')
                axi.set_ylabel(r'$I$ (A)')
                # Conductance
                axi = ax[0,2]
                axi.plot(u[ii,:-1], conductance[ii], 'o-')
                axi.set_xlabel(r'$U$ (V)')
                axi.set_ylabel(r'$G (A/V)$')
                #  axi.set_xlim([1,4])
                #  axi.set_ylim([0,20])
                # Resistance
                axi = ax[1,2]
                axi.plot(u[ii,:-1], resistance[ii], 'o-')
                axi.set_xlabel(r'$U$ (V)')
                axi.set_ylabel(r'$R = \frac{1}{G} (\Omega)$')
                axi.set_xlim([1,3])
                axi.set_ylim([0,15])
        # ----------------------------------------------

        return fig

def mapcolor(colormap, Ncolors):
    '''
    create a custom colormap from certain matplolib colormap
    # Have a look at the colormaps here and decide which one you like:
    # http://matplotlib.org/1.2.1/examples/pylab_examples/show_colormaps.html
    '''
    Ncolors = min(colormap.N,Ncolors)
    mapcolors = [colormap(int(x*colormap.N/Ncolors)) for x in range(Ncolors)]
    return mapcolors

def availableMarkers(chooseAll=False):
    '''
    '''
    if chooseAll:
        markers = list(mpl.lines.lineMarkers.keys())
        #  markers = list(map(str, markers)) # convert all markers to strings
    else:
        markers = ['o','*','^','v','<','x','>','p','s','h','H','D','d', 'p','.','']
    return markers

def plot2vars (deviceList, selDevs, xVars, yVars, xLims, yLims, colorVar, colormap=plt.cm.Spectral):
    # TODO: add a way to set conditions, e.g. by a dictionary
    #  conds = { cqe: '<1', ...}
    colorVarVals = list(selDevs[colorVar].unique())
    colors = mapcolor(colormap, len(colorVarVals))
    #  lines = iter (1000*['-','--','-.',':'])
    #  markers = iter(1000*availableMarkers (chooseAll=True))
    for diam in selDevs.diameter.unique():
        fig, ax = plt.subplots(1,2)
        fig.suptitle ('Diameter: {dia:.1e} m'.format(dia=diam))
        for dev in deviceList:
            #  lineSty = next (lines)
            #  markerSty = next (markers)
            if dev.diameter == diam and \
                    not dev.discard and \
                    not selDevs.loc[(selDevs.sampleName==dev.sampleName) &
                            (selDevs.samplePID == dev.samplePID) &
                            (selDevs.name == dev.name)].empty:
                        ax[0].plot(getattr(dev, xVars[0]), getattr(dev, yVars[0]),
                                c = colors[colorVarVals.index(getattr(dev,colorVar))],
                                #  linestyle = lineSty, marker = markerSty,
                                linestyle = dev.lineSty, marker = dev.markerSty,
                                label='S:{smp}|P:{pid}|D:{dev}'
                                   .format(smp=dev.sampleName, pid=dev.samplePID,dev=dev.name))
                        ax[0].set_xlabel(xVars[0])
                        ax[0].set_ylabel(yVars[0])
                        ax[0].set_xlim(xLims[0])
                        ax[0].set_ylim(yLims[0])
                        ax[0].legend(ncol=2, loc='best')
                        ax[1].plot(getattr(dev,xVars[1]), getattr(dev,yVars[1]),
                                c=colors[colorVarVals.index(getattr(dev,colorVar))],
                                linestyle = dev.lineSty, marker = dev.markerSty,
                                label='S:{smp}|P:{pid}|D:{dev}'
                                   .format(smp=dev.sampleName, pid=dev.samplePID,dev=dev.name))
                        ax[1].set_xlabel(xVars[1])
                        ax[1].set_ylabel(yVars[1])
                        ax[1].set_xlim(xLims[1])
                        ax[1].set_ylim(yLims[1])
                        ax[1].legend(ncol=2, loc='best')

def plotStatistics(dataFrame, x, y, hue, columnsData, title='', axisLabels = None, axisLimits = None, axisTicks = None, markerSize = 60, col_wrap = 2, palette = 'Set1', adjustSubplots = True, differentHueMarkers = False, legend_out = True, fitReg = False, savePlot=False, chooseAllAvailableMarkers = False):
    '''
    '''
    # TODO: get it to work without hue or columnsData = None

    if chooseAllAvailableMarkers:
        m_styles = availableMarkers(chooseAll=True)
    else:
        m_styles = availableMarkers(chooseAll=False)

    m_array = m_styles[0:dataFrame[hue].nunique()]

    if differentHueMarkers:
        markerSty = m_array
    else:
        markerSty = 'o'

    g = sns.lmplot (x, y,
            data = dataFrame,
            hue = hue,
            fit_reg = fitReg,
            col = columnsData,
            col_wrap = col_wrap,
            palette = palette,
            markers = markerSty,
            scatter_kws = {'s': markerSize},
            legend_out = legend_out,
            )

    g.fig.suptitle (title)

    if axisLabels:
        xLab, yLab = axisLabels
        g.set_axis_labels (xLab, yLab)

    if axisLimits:
        xLim, yLim = axisLimits
        g.set (xlim = xLim, ylim = yLim), #axisLimis = [(0, 60), (0, 12)]

    if axisTicks:
        xTicks, yTicks = axisTicks
        g.set (xticks=xTicks, yticks=yTicks)

    if adjustSubplots:
        g.fig.subplots_adjust (wspace = .02, hspace = .05)

    if savePlot:
        plt.savefig(savePlot)
        print('Figured saved in {}'.format(savePlot))

def plot3d (title, dataFrame, xParam, yParam, zParam):
    fig = plt.figure()
    plt.title(title)
    #-----------------------------------------------------------
    ax = fig.add_subplot(221, projection='3d')
    dg = dataFrame.query('Diameter == 0.000250 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    #  z = (z-np.nanmin(z)) / (np.nanmax(z) - np.nanmin(z))
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 250 $\mu$m')
    ax.grid(True)
    #-----------------------------------------------------------
    ax = fig.add_subplot(222, projection='3d')
    dg = dataFrame.query('Diameter == 0.000500 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 500 $\mu$m')
    ax.grid(True)
    #-----------------------------------------------------------
    ax = fig.add_subplot(223, projection='3d')
    dg = dataFrame.query('Diameter == 0.001000 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 1000 $\mu$m')
    ax.grid(True)
    #-----------------------------------------------------------
    ax = fig.add_subplot(224, projection='3d')
    dg = dataFrame.query('Diameter == 0.002000 & Discard == False & has4pp == False')
    x = dg[xParam]
    y = dg[yParam]
    z = dg[zParam]
    scat = ax.scatter(x, y, z, c=x, cmap = 'jet')
    fig.colorbar(scat)
    ax.set_xlabel(xParam)
    ax.set_ylabel(yParam)
    ax.set_zlabel(zParam)
    ax.set_title('Diameter = 2000 $\mu$m')
    ax.grid(True)

def plotCDist(deviceList, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title, chosenSample, selDevs, colormap=plt.cm.Spectral, Ncolors=8, savePlot=False, is4pp=False):
    '''
    Arguments
    '''

    # TODO: explore devices per sampleName and PID and create a selection of the best with selecDevs()

    #  selDevs = fa.selectDevs(case, sampleName, samplePID, deviceList)

    colorDist = setColors(dT, dM)
    linestyDist = setLinestyle(dT, dM)

    fig, ax = plt.subplots(3,2)
    fig.suptitle(chosenSample+' '+title)

    for ii in range(len(deviceList)):

        if not discard[ii] and \
                sampleName[ii] == chosenSample and \
                sampleName[ii] in selDevs and \
                samplePID[ii] in selDevs[sampleName[ii]] and \
                deviceList[ii] in selDevs[sampleName[ii]][samplePID[ii]]:

            if diameter[ii] == 0.00025:
                ax0 = ax[0,0]
                ax1 = ax[0,1]
            elif diameter[ii] == 0.00050:
                ax0 = ax[1,0]
                ax1 = ax[1,1]
            elif diameter[ii] == 0.00100:
                ax0 = ax[2,0]
                ax1 = ax[2,1]

            ax0.semilogy(u[ii], abs(iL[ii]),
                    color = colorDist[ii], linestyle = linestyDist[ii], #marker = markerDist[ii],
                    label = r'%s-%s-%s: Rs=%2.1f (dT = %.0f, dM = %.0f)' %(sampleName[ii], samplePID[ii], devID[ii], rs[ii], dT[ii], dM[ii])
            )
            ax1.plot(u[ii], cqe[ii],
                    color = colorDist[ii], linestyle = linestyDist[ii], #marker = markerDist[ii],
                    #  color = color, linestyle = linestyle, marker = marker,
                    label = r'%s-%s-%s: Rs=%0.1f (dT = %.0f, dM = %.0f)' %(sampleName[ii], samplePID[ii], devID[ii], rs[ii], dT[ii], dM[ii])
            )
            #--------------
            if diameter[ii] != 0.00100:
                ax0.xaxis.set_visible(False)
            ax0.set_xlabel(r'$U$ (V)')
            ax0.set_ylabel(r'$I_L$')
            ax0.set_xlim([1.0, 4.5])
            ax0.set_ylim([1e-4,5e-1])
            ax0.set_title('Diameter=%i $\mu$m' %(1e6*diameter[ii]))
            #--------------
            if diameter[ii] != 0.00100:
                ax1.xaxis.set_visible(False)
            ax1.set_xlabel(r'$U$ (V)')
            ax1.set_ylabel(r'$CQE$')
            ax1.set_xlim([1.0, 4.5])
            ax1.set_ylim([0.3,0.7])
            ax0.legend(ncol=1,
                    loc='best',
                    columnspacing=1.0, labelspacing=0.0,
                    handletextpad=0.0, handlelength=1.5,
                    fancybox=True, shadow=True,
                    prop={'size':6},
            )
    #  fig.tight_layout()
    if is4pp:
        fig, ax = plt.subplots(2,1)
        ax0 = ax[0]
        ax1 = ax[1]
        #  for ii,(color, linestyle, marker) in zip(range(N),itertools.product(mapcolors, l_styles, m_styles)):
        for ii in range(len(deviceList)):
            if not discard[ii] and \
                    sampleName[ii] == chosenSample and \
                    sampleName[ii] in selDevs and \
                    samplePID[ii] in selDevs[sampleName[ii]] and \
                    deviceList[ii] in selDevs[sampleName[ii]][samplePID[ii]]:

                ax0.semilogy(u[ii], abs(iL[ii]),
                        color = colorDist[ii], linestyle = linestyDist[ii],
                        #  color = color, linestyle = linestyle, marker = marker,
                        label = r'%s-%s-%s: Rs=%2.1f (dT = %.0f, dM = %.0f)' %(sampleName[ii], samplePID[ii], devID[ii], rs[ii], dT[ii], dM[ii])
                )
                ax1.plot(u[ii], cqe[ii],
                        color = colorDist[ii], linestyle = linestyDist[ii], #marker = markerDist[ii],
                        #  color = color, linestyle = linestyle, marker = marker,
                        label = r'%s-%s-%s: Rs=%0.1f (dT = %.0f, dM = %.0f)' %(sampleName[ii], samplePID[ii], devID[ii], rs[ii], dT[ii], dM[ii])
                )
        ax0.set_xlabel(r'$U$ (V)')
        ax0.set_ylabel(r'$I_L$')
        ax0.set_xlim([1.0, 2.5])
        ax0.set_ylim([1e-5,1e0])
        ax0.xaxis.set_visible(False)
        ax1.set_xlabel(r'$U$ (V)')
        ax1.set_ylabel(r'$CQE$')
        ax1.set_xlim([1.0, 2.5])
        ax1.set_ylim([0.3,0.7])
        fig.suptitle(chosenSample+' '+title)
        ax0.legend(ncol=1,
                loc='best',
                columnspacing=1.0, labelspacing=0.0,
                handletextpad=0.0, handlelength=1.5,
                fancybox=True, shadow=True,
                prop={'size':8},
        )

    if savePlot:
        fig.savefig(savePlot)

def setColors(dT, dM, colorSet='default', parameterizeBy='dT'):
    if parameterizeBy == 'both':
    # Set a color for each contact distance
        # 2nd gen mask
        colorDist = np.where ((dT ==  5) & (dM ==  5), 'black', 'w')
        colorDist = np.where ((dT == 10) & (dM == 10), 'darkgoldenrod', colorDist)
        colorDist = np.where ((dT == 20) & (dM == 20), 'blue', colorDist)
        colorDist = np.where ((dT == 40) & (dM == 40), 'olivedrab', colorDist)
        # 3rd gen mask
        colorDist = np.where ((dT == 50) & (dM == 25), 'steelblue', colorDist)
        colorDist = np.where ((dT == 50) & (dM == 50), 'steelblue', colorDist)
        colorDist = np.where ((dT == 75) & (dM == 25), 'darkred', colorDist)
        colorDist = np.where ((dT == 75) & (dM == 75), 'darkred', colorDist)
        colorDist = np.where ((dT == 95) & (dM ==  5), 'darkgreen', colorDist)
        colorDist = np.where ((dT == 95) & (dM == 95), 'darkgreen', colorDist)
    elif parameterizeBy == 'dT':
        if colorSet == 'greyscale':
            # Greyscale np.linspace (0, 1, 8)
            colorDist = np.where ((dT == 95), '0.14285714', 'w')
            colorDist = np.where ((dT == 75), '0.28571429', colorDist)
            colorDist = np.where ((dT == 50), '0.42857143', colorDist)
            colorDist = np.where ((dT == 40), '0.57142857', colorDist)
            colorDist = np.where ((dT == 20), '0.71428571', colorDist)
            colorDist = np.where ((dT == 10), '0.85714286', colorDist)
            colorDist = np.where ((dT ==  5), '1.00000000', colorDist)
        if colorSet == 'default':
            colorDist = np.where ((dT == 95), 'blue', 'w')
            colorDist = np.where ((dT == 75), 'orange', colorDist)
            colorDist = np.where ((dT == 50), 'green', colorDist)
            colorDist = np.where ((dT == 40), 'red', colorDist)
            colorDist = np.where ((dT == 20), 'purple', colorDist)
            colorDist = np.where ((dT == 10), 'olive', colorDist)
            colorDist = np.where ((dT ==  5), 'gray', colorDist)

    return colorDist

def setLinestyle (dT, dM):
    # By dM
    linestyDist = np.where ((dT ==  5) & (dM ==   5), '-', '')
    linestyDist = np.where ((dT == 10) & (dM ==  10), '-', linestyDist)
    linestyDist = np.where ((dT == 20) & (dM ==  20), '-', linestyDist)
    linestyDist = np.where ((dT == 40) & (dM ==  40), '-', linestyDist)
    linestyDist = np.where ((dT == 50) & (dM ==  25), ':', linestyDist)
    linestyDist = np.where ((dT == 50) & (dM ==  50), '-', linestyDist)
    linestyDist = np.where ((dT == 75) & (dM ==  25), ':', linestyDist)
    linestyDist = np.where ((dT == 75) & (dM ==  75), '-', linestyDist)
    linestyDist = np.where ((dT == 95) & (dM ==   5), ':', linestyDist)
    linestyDist = np.where ((dT == 95) & (dM ==  95), '-', linestyDist)
    return linestyDist

