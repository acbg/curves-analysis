import numpy as np
import pandas as pd

def get_data(output, inFiles):
    '''
    Import data from a set of files
    '''

# Define objects
# --------------
for obj in output:
    obj = np.array(np.empty(len(inFiles), dtype=object)

            '''
u = np.array(np.empty(len(filelist)),dtype=object)
uD = np.array(np.empty(len(filelist)),dtype=object)
iL = np.array(np.empty(len(filelist)),dtype=object)
iD = np.array(np.empty(len(filelist)),dtype=object)
uc = np.array(np.empty(len(filelist)),dtype=object)
uDc = np.array(np.empty(len(filelist)),dtype=object)
iLc = np.array(np.empty(len(filelist)),dtype=object)
iDc = np.array(np.empty(len(filelist)),dtype=object)
samplePID = np.array(np.empty(len(filelist)),dtype=object) # Sample Process ID
sampleName = np.array(np.empty(len(filelist)),dtype=object) # Sample Name
dia = np.array(np.empty(len(filelist)),dtype=object)
device = np.array(np.empty(len(filelist)),dtype=object)
devID = np.array(np.empty(len(filelist)),dtype=object)
T = np.array(np.empty(len(filelist)),dtype=object)
meas = np.array(np.empty(len(filelist)),dtype=object)
comments = np.array(np.empty(len(filelist)),dtype=object)
'''

# Read the files
# --------------
for ii in range(len(filelist)):
    filename = filelist[ii]

    #  Read each csv file and add the IV data
    dfi = pd.read_csv(filename, delimiter=',', comment='#')

    # Store data in separated numpy ndarrays
    iL[ii] =  dfi['IS_pulse'].values if dfi['IS_pulse'].values[-1] > 0 else -dfi['IS_pulse'].values
    u[ii] =  dfi[' VS_pulse'].values
    iD[ii] =  dfi[' ID_pulse'].values if dfi[' ID_pulse'].values[-1] > 0 else -dfi[' ID_pulse'].values
    uD[ii] =  dfi[' VD_pulse'].values
    #  iLc[ii] =  dfi['IS_cont'].values if dfi['IS_cont'].values[-1] > 0 else - dfi['IS_cont'].values

    #  Get parameters from filename and store them in ndarrays
    with filename.open() as f:
        try:
            comment = f.readlines()[2]
            comment = comment.split('# ', 1)[-1]
            comment = comment.split('\n', 1)[0]
            comments[ii] = comment
        finally:
            f.close()

    sampID = filename.parent.as_posix().split('IVdata')[0]
    samplePID[ii] = sampID.split('/')[-2]
    sampleName[ii] = sampID.split('/')[-3]

    devID[ii] = filename.stem

    diam = re.split('[A-Z]+', devID[ii])[0]
    dia[ii] = 1e-6 * float(diam) # in meters

    dev = re.findall('[a-zA-Z]+[0-9]', devID[ii])[0]
    device[ii] = dev

    meas[ii] = re.findall('[a-zA-Z]', devID[ii])[-1]

    T[ii] = 300


