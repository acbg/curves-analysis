import time
st=time.time() # count time from here
import os
import glob
import pandas as pd
def read_files(path, mask, extension, output_df):
    '''
    Read multiple files
    '''
# ----------
print("****** Elapsed time: %.2f s (Load files)"%(time.time()-st))

initial_path = os.getcwd() # read initial directory
os.chdir('../../../../ZIM/notes/iTPX/Samples/') # change directory to the root of samples
path = r'G/Aalto3.1/S125/IVdata/' # select which samples you want
filelist = glob.glob(os.path.join(path, "*.dat"))


df = pd.DataFrame()
for ii in range(len(filelist)):
    print('file:', filelist[ii])

    '''
    # Store observables as arrays
    # ---------------------------
    # Create DataFrame cells as arrays from csv columns
    df.loc[ii,'iL']  = file_df[ 'IS_pulse'].values
    df.loc[ii,'iD']  = file_df[' ID_pulse'].values
    df.loc[ii,'u']   = file_df[' VS_pulse'].values
    df.loc[ii,'uD']  = file_df[' VD_pulse'].values
    df.loc[ii,'iLc'] = file_df[' IS_cont' ].values
    df.loc[ii,'iDc'] = file_df[' ID_cont' ].values
    df.loc[ii,'uc']  = file_df[' VS_cont' ].values
    df.loc[ii,'uDc'] = file_df[' VD_cont' ].values
    '''

    #  Read each csv file and add the IV data
    headers = ['iL', 'u', 'iD', 'uD', 'iLc', 'uc', 'iDc', 'uDc']
    file_df = pd.read_csv(filelist[ii], delimiter=',', comment='#', skiprows=22, names=headers)
    if file_df['iL'].iloc[-1] < 0:
        file_df['iL'] = - file_df['iL']
    if file_df['iD'].iloc[-1] < 0:
        file_df['iD'] = - file_df['iD']
    if file_df['iLc'].iloc[-1] < 0:
        file_df['iLc'] = - file_df['iLc']
    if file_df['iDc'].iloc[-1] < 0:
        file_df['iDc'] = - file_df['iDc']

    #  Get new parameters from filename and add them to new columns in the dataframe
    string = filelist[ii].replace(".dat","");
    sample = re.compile('/\D+\d.\d-\D\d\d\d').findall(string)[0];
    sample = re.sub('/','', sample)
    file_df['sample'] = sample
    if re.search('-\D125', sample):
        maskGen = 2
    else:
        maskGen = 3
    file_df['maskGen'] = maskGen
    diam = re.compile('\d+\d').findall(string)[-1]
    file_df['diam'] = int(diam)
    dev = re.compile('\D+\d').findall(string)[-1]
    file_df['dev'] = dev
    file_df['T'] = 300
    #dTM (distance between Top contact and top Mesa)
    #dMM (distance between Middle contact and top Mesa)
    if maskGen == 3:
        if re.search('[a-bA-B][0-9]', dev):
            file_df['dTM'] = 50
            if re.search('[0-4]', dev):
                file_df['dMM'] = 25
            else:
                file_df['dMM'] = 50
        elif re.search('[c-dC-D][0-9]', dev):
            file_df['dTM'] = 75
            if re.search('[0-4]', dev):
                file_df['dMM'] = 25
            else:
                file_df['dMM'] = 75
        elif re.search('[e-fE-F][0-9]', dev):
            file_df['dTM'] = 95
            if re.search('[0-4]', dev):
                file_df['dMM'] = 5
            else:
                file_df['dMM'] = 95
    elif maskGen == 2:
        if re.search('[C-H]', dev):
            if re.search('[0-4]', dev):
                file_df['dTM'] = file_df['dMM'] = 5
            elif re.search('[5-9]', dev):
                file_df['dTM'] = file_df['dMM'] = 10
        if re.search('[J-L]', dev):
            if re.search('[0-4]', dev):
                file_df['dTM'] = file_df['dMM'] = 20
            elif re.search('[5-9]', dev):
                file_df['dTM'] = file_df['dMM'] = 40

    # Append extracted dataframe to the general df
    df = df.append(file_df)

# Go back to initial directory
os.chdir(initial_path)

print("****** Elapsed time: %.2f s (Loaded %.f parameters from %.f files)" %(time.time()-st, df.shape[1], len(filelist)))
