#/usr/bin/env python3

##################################
# Analysis of experimental curves
##################################

###############################################################################
print("*** Elapsed time: 0.00 s (Initialize: import modules)")
###############################################################################

import time # time tracking
st=time.time() # count time from here
import pathlib
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from pprint import pprint
import timeit

import functionsArrays as f
#  import funs.functions as f
#  from analysisCDist import *

# Numpy settings
# --------------
np.warnings.filterwarnings('ignore') #ignore when comparing NaNs
np.set_printoptions(threshold=np.inf)

# Pandas settings
# ---------------
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 0)

#  Seaborn settings
#  ----------------
sns.reset_orig ()
sns.set (context='notebook',
        style='darkgrid',
        palette='dark',
        font='serif',
        font_scale=1.0,
        color_codes=True,
        rc=None
        )

#  Matplotlib settings
#  ----------------------------
#  print(plt.style.available)
#  plt.style.use('ggplot')
#  plt.style.use('dark_background')
#  mpl.style.use('ggplot')
#  plt.rcParams.update({
#      'font.size': 17,
#      'font.family': 'serif',
#      'xtick.labelsize' : 17,
#      'ytick.labelsize' : 17,
#      })

###############
# Import files
###############

# Choose the root directory ...
rootDir = f.getRootDir('') # defaults to /m/nbe/project/quantum-data/ZIM/notes/iTPX/Samples/
#  rootDir = f.getRootDir(str(pathlib.Path.home())+'/qdata/ZIM/notes/iTPX/Samples/')

# ... and subdirectories according to the analysis to perform
case = 'PV' #Analysis of photodetectors
case = 'ContactDistance' # Analysis of the effect of contact distance on DDS
dirList = f.getDirList(case)

# Select file extensions to look for
mask = ['*.dat']

# And create a file list
filelist = f.getFiles (rootDir, dirList, mask, showFiles=False)


###############################################################################
print("*** Elapsed time: %.2f s (Extract data from files)"%(time.time()-st))
###############################################################################
q, k = np.genfromtxt('constants.csv')


# Data
# ----
u, uD, iL, iD, uc, uDc, iLc, iDc, has4pp, u4pp, uD4pp,\
        iL4pp, iD4pp, u4ppc, uD4ppc, iL4ppc, iD4ppc, T,\
        samplePID, sampleName, diameter, device, devID,\
        meas, comments, ARthickness, isPassivated, isDark =\
            f.getParams(filelist)

# Constants
# ---------
q, k = np.genfromtxt('constants.csv')
T = 300 #TODO modify functions to be able to handle a T array
ni = f.intrinsicCarrierDensity('GaAs', T)

###############################################################################
print("*** Elapsed time: %.2f s (Calculate and compute new parameters)"
        %(time.time()-st))
###############################################################################

# Calculate parameters
# --------------------
area, jL, jL4pp, cond, condMax, resistance, rs, cond4pp,\
        condMax4pp, resistance4pp, rs4pp, ideality, ideality4pp,\
        uL, uL4pp, srh, srh4pp, cqe, cqem, cqemI, cqemV, cqec,\
        cqe4pp, cqem4pp, cqemI4pp, cqemV4pp, cqec4pp, pce, pcem,\
        pce4pp, pcem4pp, iRev, iFor, discard =\
            f.calculateParams (
                'GaAs', u, uD, iL, iD, uc, uDc, iLc, iDc, has4pp,
                u4pp, uD4pp, iL4pp, iD4pp, u4ppc, uD4ppc, iL4ppc, iD4ppc,
                T, samplePID, sampleName, diameter, device, devID, meas,
                comments, ARthickness, isPassivated, isDark
            )

###############################################################################
print("*** Elapsed time: %.2f s (Discard defective devices)" %(time.time()-st))
###############################################################################

nDevs = discard.shape[0]
nAutoDiscardDevs = np.count_nonzero(discard)

# Discard visually-detected defective individual curves
discard = f.discardManual (
        case, discard, sampleName, samplePID, devID, device
        )

nManualDiscardDevs = np.count_nonzero(discard)

###############################################################################
print("*** Elapsed time: %.2f s (%i devices: %i discarded, of which %i were\
 automatically discarded)"
        %(time.time()-st, nDevs, nManualDiscardDevs, nAutoDiscardDevs))
###############################################################################

###############################################################################
print("*** Elapsed time: %.2f s (Create DataFrame)" %(time.time()-st))
###############################################################################

# Create a dataframe and new arrays
# ---------------------------------
allDevsDF, summary, table, MaskGen, dT, dM, dC =\
        f.createDF (
            sampleName, samplePID, diameter, T, devID, device, meas, comments,
            isPassivated, discard, area, iRev, iFor, rs, condMax, cqem, cqec,
            cqemI, cqemV, pcem, srh, ideality, has4pp, rs4pp, cqem4pp, cqec4pp,
            cqemI4pp, cqemV4pp, pcem4pp, srh4pp, ideality4pp,
            saveTable=True, fileName='output/summary.txt', printTable=False
        )

###############################################################################
print("*** Elapsed time: %.2f s (Select devices)" %(time.time()-st))
###############################################################################

# If we dont want to discard anything:
#  discard = np.where (abs(iRev) > 1e-7, False, False)

# Drop discarded devices and those with a CQE higher or equal to 1:
allDevsDF = allDevsDF[(allDevsDF['CQEm'] < 1) & (allDevsDF['Discard'] == False)]

# Select best devices...

# ...considering just one variable:
#  bestDevs = f.selDevs1var (
#          allDevsDF,
#          ['Device','SamplePID'],
#          'CQEm',
#          'Sample', 'SamplePID', 'Diameter', 'dT', 'dM',)
#  print(bestDevs)

# ...or two variables:
bestDevs = f.selDevs2vars (
        allDevsDF,
        ['Device'],
        'CQEm', 'CQEm',
        'Sample', 'SamplePID', 'Diameter',
        )

# And create a dictionary with the selection:
bestDevsDict = f.df2dict (
        df = bestDevs,
        maingroup='Sample', subgroup='SamplePID', selectedCol='Device'
        )
#  pprint(bestDevsDict)


###############################################################################
print("*** Elapsed time: %.2f s (Plot figures)" %(time.time()-st))
###############################################################################

# General plots
# -------------
#  f.plotEachDDS(device, devID, [dT, dM], u, [iL,iD, cqe, pce], [rs, 0, cqem, pcem], sty='v--')

#  f.plotResistance(device, samplePID, u, uL, iL, cond, resistance, rs, discard)
#  f.plotResistance(device, samplePID, u4pp, uL4pp, iL4pp, cond4pp, resistance4pp, rs4pp, discard, array4pp=has4pp, plot4pp=True)

#  dg = allDevsDF.query('Discard == False & has4pp == False')
#  f.plotStatistics(
#          dataFrame = dg, x = 'dM', y = 'CQEm', hue = 'dT', columnsData = 'Diameter',
#          palette = 'Dark2', markerSize = 80, #differentHueMarkers = True,
#          title = '2pp', axisLabels = ["$d_{T}$ ($\mu$m)", "$CQE_{max}$"],
#          #  axisLimits = [(0, 100), (0, 1)], axisTicks=[[10, 30, 50], [2, 6, 10]],
#          #  savePlot = 'output/test.svg',
#          )

#  f.plot3d ('CQE(2pp)', allDevsDF, 'dT', 'dM', 'PCEm')

#  f.plotResistance(device, samplePID, u, uL, iL, cond, resistance, rs, discard)

c_styles = f.mapcolor(plt.cm.Spectral, 9)
l_styles = ['-','--','-.',':']
m_styles = f.availableMarkers()

# Plot all devices in the analysis
#  f.plotAllDevs (u, uL, iL, iD, cqe, pce, c_styles, l_styles, m_styles, diameter, discard, device, sampleName, samplePID, devID, selDevs=None, showPD=False, savePlotDir='')
#  f.plotIVAllDevs (u4pp, uL, iL4pp, iD, cqe4pp, pce, mapcolors, l_styles, m_styles, diameter, discard, device, sampleName, samplePID, devID, chosenSample='TPX0342')

# Analysis specific plots
# -----------------------
#  f.plotAllDevs (u, uL, iL, uL, cqe, cqem, pce, rs, c_styles, l_styles, m_styles, dT, dM, diameter, discard, device, sampleName, samplePID, devID, chosenDT = 5, chosenDM = 5, chosenSample='Aalto3.1', savePlotDir='output/')

# Auto selection of bestdevices for 4pp
bestDevs = f.selDevs2vars ( allDevsDF,
        ['Device'],
        'CQEm', 'CQEm',
        'Sample', 'SamplePID', 'Diameter', 'dT', 'dM')
#  print(bestDevs)

bestDevsDict = f.df2dict (df = bestDevs,
        maingroup='Sample', subgroup='SamplePID', selectedCol='Device')
#  pprint(bestDevsDict)

# TODO: improve the plotcdist function
#  f.plotCDistNew(selDevs, selSample, title, deviceList, diameter, devID, sampleName, samplePID, discard, xVars=[u, u], yVars=[iL, cqe], yVarsLeg=[rs, cqem], diffVars=[dT, dM], savePlotDir=False)

f.plotCDist (device, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title='2pp (auto selection)', chosenSample='Aalto3.1', selDevs=bestDevsDict, savePlot='output/exp-Aalto3.1-2pp-autoSel.svg')
f.plotCDist (device, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title='2pp (auto selection)', chosenSample='TPX0341', selDevs=bestDevsDict, savePlot='output/exp-TPX0341-2pp-autoSel.svg')
f.plotCDist (device, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title='2pp (auto selection)', chosenSample='TPX0342', selDevs=bestDevsDict, savePlot='output/exp-TPX0342-2pp-autoSel.svg')

# Auto selection of bestdevices for 4pp
bestDevs = f.selDevs2vars ( allDevsDF,
        ['Device'],
        'CQEm4pp', 'CQEm4pp',
        'Sample', 'SamplePID', 'Diameter', 'dT', 'dM')

# And create a dictionary with the selection:
bestDevsDict = f.df2dict ( df = bestDevs,
        maingroup='Sample', subgroup='SamplePID', selectedCol='Device')
#  pprint(bestDevsDict)
f.plotCDist (device, diameter, devID, u4pp, iL4pp, iD4pp, cqe4pp, cqem4pp, pce4pp, pcem4pp, dT, dM, rs, sampleName, samplePID, discard, title='4pp (auto selection)', chosenSample='TPX0341', selDevs=bestDevsDict, savePlot='output/exp-TPX0341-4pp-autoSel.svg', is4pp=True)
f.plotCDist (device, diameter, devID, u4pp, iL4pp, iD4pp, cqe4pp, cqem4pp, pce4pp, pcem4pp, dT, dM, rs, sampleName, samplePID, discard, title='4pp (auto selection)', chosenSample='TPX0342', selDevs=bestDevsDict, savePlot='output/exp-TPX0342-4pp-autoSel.svg', is4pp=True)

# Manually selected
bestDevsDict=f.selectedDevs('CDist2pp')
f.plotCDist (device, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title='2pp', chosenSample='Aalto3.1', selDevs=bestDevsDict, savePlot='output/exp-Aalto3.1-2pp.svg')
f.plotCDist (device, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title='2pp', chosenSample='TPX0341', selDevs=bestDevsDict, savePlot='output/exp-TPX0341-2pp.svg')
f.plotCDist (device, diameter, devID, u, iL, iD, cqe, cqem, pce, pcem, dT, dM, rs, sampleName, samplePID, discard, title='2pp', chosenSample='TPX0342', selDevs=bestDevsDict, savePlot='output/exp-TPX0342-2pp.svg')

bestDevsDict=f.selectedDevs('CDist4pp')
f.plotCDist (device, diameter, devID, u4pp, iL4pp, iD4pp, cqe4pp, cqem4pp, pce4pp, pcem4pp, dT, dM, rs, sampleName, samplePID, discard, title='4pp', chosenSample='TPX0341', selDevs=bestDevsDict, savePlot='output/exp-TPX0341-4pp.svg', is4pp=True)
f.plotCDist (device, diameter, devID, u4pp, iL4pp, iD4pp, cqe4pp, cqem4pp, pce4pp, pcem4pp, dT, dM, rs, sampleName, samplePID, discard, title='4pp', chosenSample='TPX0342', selDevs=bestDevsDict, savePlot='output/exp-TPX0342-4pp.svg', is4pp=True)

# Show plots
# ----------
plt.show()

##############################################################################
print("*** Elapsed time: %.2f s (End of script)" %(time.time()-st))
##############################################################################
